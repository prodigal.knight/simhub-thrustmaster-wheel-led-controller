﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  public class SemaphoreLock {
    private SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);
    private bool locked = false;

    public bool IsLocked => this.locked;

    /** Lock this semaphore */
    public async Task RequestLock() {
      await this.semaphore.WaitAsync();
      this.locked = true;
    }

    /** Lock this semaphore for the duration of the given callback */
    public async void RequestLockWhile(Func<Task> callback) {
      await this.RequestLock();

      try {
        await callback();
      } finally {
        this.Unlock();
      }
    }

    /** Unlock this semaphore */
    public void Unlock() {
      if (this.locked) {
        this.locked = false;
        this.semaphore.Release();
      }
    }

    public void Reset() {
      this.semaphore.Dispose();
      this.semaphore = new SemaphoreSlim(1, 1);
      this.locked = false;
    }
  }
}
