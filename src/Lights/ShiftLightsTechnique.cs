﻿using System;

namespace SimHub.ThrustmasterLEDControllerPlugin.Lights {
  public class ShiftLightsTechnique {
    /** Rev LEDs go from left to right linearly */
    public static ShiftLightsTechnique ltr(int numLeds) => new ShiftLightsTechnique(
      "leftToRight",
      (rpmPct, minRpmPct, maxRpmPct, nightMode) => {
        ushort ledState = 0x0000;
        ushort nightModeLedState = 0x0000;

        double rpmPctPerLed = (maxRpmPct - minRpmPct) / numLeds;
        double minPctForIllumination = minRpmPct;
        for (byte i = 0; i < numLeds; i++, minPctForIllumination += rpmPctPerLed) {
          if (rpmPct >= minPctForIllumination) {
            nightModeLedState = (ushort)(0x0001 << i);
            ledState |= nightModeLedState;
          }
        }

        return nightMode ? nightModeLedState : ledState;
      }
    );

    /** Rev LEDs go from edges towards center linearly */
    public static ShiftLightsTechnique etc(int numLeds) => new ShiftLightsTechnique(
      "edgeToCenter",
      (rpmPct, minRpmPct, maxRpmPct, nightMode) => {
        ushort ledState = 0x0000;
        ushort nightModeLedState = 0x0000;

        int halfNumLeds = numLeds / 2 + 1;
        double rpmPctPerLed = (maxRpmPct - minRpmPct) / halfNumLeds;
        double minPctForIllumination = minRpmPct;
        for (
          byte i = 0, j = (byte)(numLeds - 1);
          i < halfNumLeds;
          i++, j--, minPctForIllumination += rpmPctPerLed
        ) {
          if (rpmPct >= minPctForIllumination) {
            nightModeLedState = (ushort)(1 << i | 1 << j);
            ledState |= nightModeLedState;
          }
        }

        return nightMode ? nightModeLedState : ledState;
      }
    );

    /** Rev LEDs go from center towards edges linearly */
    public static ShiftLightsTechnique cte(int numLeds) => new ShiftLightsTechnique(
      "centerToEdge",
      (rpmPct, minRpmPct, maxRpmPct, nightMode) => {
        ushort ledState = 0x0000;
        ushort nightModeLedState = 0x0000;

        int halfNumLeds = numLeds / 2;
        double rpmPctPerLed = (maxRpmPct - minRpmPct) / halfNumLeds;
        double minPctForIllumination = minRpmPct;
        for (
          byte i = (byte) halfNumLeds, j = (byte) halfNumLeds, count = 0;
          count < halfNumLeds + 1;
          i--, j++, minPctForIllumination += rpmPctPerLed, count++
        ) {
          if (rpmPct >= minPctForIllumination) {
            nightModeLedState = (ushort)(1 << i | 1 << j);
            ledState |= nightModeLedState;
          }
        }

        return nightMode ? nightModeLedState : ledState;
      }
    );

    /** Rev LEDs go from left to right in thirds (e.g. for 15-led wheel 5 illuminate at a time) */
    public static ShiftLightsTechnique ltrThirds(int numLeds) {
      ushort ledIlluminationPerThird = 0x0000;

      int oneThirdLeds = numLeds / 3;

      for (byte i = 0; i < oneThirdLeds; i++) {
        ledIlluminationPerThird |= (ushort)(0x0001 << i);
      }

      return new ShiftLightsTechnique(
        "leftToRightThirds",
        (rpmPct, minRpmPct, maxRpmPct, nightMode) => {
          ushort ledState = 0x0000;
          ushort nightModeLedState = 0x0000;

          double rpmPctPerLed = (maxRpmPct - minRpmPct) / 3;
          double minPctForIllumination = minRpmPct;
          for (byte i = 0; i < 3; i++, minPctForIllumination += rpmPctPerLed) {
            if (rpmPct >= minPctForIllumination) {
              nightModeLedState = (ushort)(ledIlluminationPerThird << oneThirdLeds * i);
              ledState |= nightModeLedState;
            }
          }

          return nightMode ? nightModeLedState : ledState;
        }
      );
    }

    // FUTURE: Add KERS techniques

    public static readonly ShiftLightsTechnique None = new ShiftLightsTechnique("none", (_1, _2, _3, _4) => 0);

    public readonly string                                     name;
    public readonly Func<double, double, double, bool, ushort> illuminate;

    public ShiftLightsTechnique(string name, Func<double, double, double, bool, ushort> illuminate) {
      this.name       = name;
      this.illuminate = illuminate;
    }
  }
}
