﻿using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Util;
using SimHub.ThrustmasterLEDControllerPlugin.Wheels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SimHub.ThrustmasterLEDControllerPlugin.Controls {
  public partial class ShiftLightsControl : UserControl, INotifyPropertyChanged {
    public static readonly DependencyProperty PluginProperty = DependencyProperty.Register(
      nameof(Plugin),
      typeof(ThrustmasterLEDControllerPlugin),
      typeof(ShiftLightsControl)
    );
    public static readonly DependencyProperty WheelProperty = DependencyProperty.Register(
      nameof(Wheel),
      typeof(Wheel),
      typeof(ShiftLightsControl)
    );

    public ThrustmasterLEDControllerPlugin Plugin {
      get => (ThrustmasterLEDControllerPlugin) GetValue(PluginProperty);
      set => SetValue(PluginProperty, value);
    }

    public Wheel Wheel {
      get => (Wheel) GetValue(WheelProperty);
      set => SetValue(WheelProperty, value);
    }
      
    public RevLeds RevLeds => this.Wheel?.RevLeds;

    public byte   Brightness {
      get => this.RevLeds?.Brightness ?? 50;
      set {
        this.RevLeds.Brightness = value;
        this.Wheel.SetBrightness(value);
      }
    }


    public List<string> ShiftLightTechniques     => this.Wheel?.AvailableShiftLightTechniques ?? new List<string>();
    public int          ShiftLightTechniqueIndex => Math.Max(
      this.ShiftLightTechniques.IndexOf(this.Wheel?.ShiftLightTechnique ?? "leftToRight"),
      0
    );

    public double MinRpmPercent {
      get => (this.RevLeds?.MinRpmPct ?? 0.5) * 100.0;
      set => this.RevLeds.MinRpmPct = ((byte) value) / 100.0;
    }

    public double MaxRpmPercent {
      get => (this.RevLeds?.MaxRpmPct ?? 1.0) * 100.0;
      set => this.RevLeds.MaxRpmPct = ((byte) value) / 100.0;
    }

    public string RpmScalingMethod {
      get => this.RevLeds?.ScalingMethod ?? "linear";
      set => this.RevLeds.ScalingMethod = value;
    }
    

    public List<string> RpmScalingMethods     => RpmScalingTechnique.techniques
      .Select(technique => technique.name)
      .ToList();
    public int          RpmScalingMethodIndex => Math.Max(this.RpmScalingMethods.IndexOf(this.RpmScalingMethod), 0);
    
    public NightMode NightMode {
      get => this.RevLeds?.NightMode ?? NightMode.Off;
      set => this.RevLeds.NightMode = value;
    }

    public List<NightMode> NightModes => new List<NightMode>(){NightMode.Off, NightMode.Auto, NightMode.On};
    public int NightModeIndex => Math.Max(this.NightModes.IndexOf(this.NightMode), 0);

    public bool Expanded { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;
    
    private readonly DebounceDispatcher brightnessDebouncer = new DebounceDispatcher(500);

    private bool hasLedLock = false;

    public ShiftLightsControl() {
      InitializeComponent();
    }
    
    /** Begin previewing LED brightness changes */
    private async void OnBrightnessSliderMouseLeftButtonDown(object sender, RoutedEventArgs e) {
      if (!this.hasLedLock) {
        await this.Plugin.LedLock.RequestLock();
        this.hasLedLock = true;

        this.Wheel.ClearAnimation();

        await this.Wheel.TurnOnLEDs();
      }
    }

    /** Finish previewing LED brightness changes after 500 milliseconds */
    private void OnBrightnessSliderMouseLeftButtonUp(object sender, RoutedEventArgs e) {
      Action callback = () => {
        _ = this.Wheel.TurnOffLEDs();

        this.hasLedLock = false;
        this.Plugin.LedLock.Unlock();
      };

      this.brightnessDebouncer.Debounce(() => Dispatcher.BeginInvoke(callback));
    }

    /** Preview the selected shift lights technique */
    private void OnShiftLightsSettingChangedPreviewTechnique(string techniqueName) {
      this.Wheel.CancelCurrentShiftLightsTechniquePreview();

      this.Plugin.LedLock.RequestLockWhile(() => this.Wheel.PreviewShiftLightsTechnique(
        techniqueName,
        this.Plugin.NightDayDetector
      ));
    }

    /** Preview the selected shift lihts technique */
    private void OnShiftLightsTechniqueChangedPreviewTechnique(object sender, SelectionChangedEventArgs e) {
      if (e.RemovedItems.Count != 0) {
        string newTechnique = e.AddedItems[0] as string;
        this.Wheel.ShiftLightTechnique = newTechnique;
        this.OnShiftLightsSettingChangedPreviewTechnique(newTechnique);
      }
    }

    /** Preview current shift lights technique if night mode setting has changed */
    private void OnShiftLightsTechniqueNightModeChangedPreviewTechnique(object sender, SelectionChangedEventArgs e) {
      if (e.RemovedItems.Count != 0 && this.Expanded) {
        this.NightMode = (NightMode) e.AddedItems[0];
        this.OnShiftLightsSettingChangedPreviewTechnique(this.Wheel.ShiftLightTechnique);
      }
    }

    /** Preview current shift lights technique if RPM scaling method is changed */
    private void OnRpmScalingMethodChangedPreviewTechnique(object sender, SelectionChangedEventArgs e) {
      if (e.RemovedItems.Count != 0) {
        this.RpmScalingMethod = e.AddedItems[0] as string;
        this.OnShiftLightsSettingChangedPreviewTechnique(this.Wheel.ShiftLightTechnique);
      }
    }

    public void NotifyAllPropertiesChanged() {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
    }

    private void NotifyPropertyChanged(params string[] propertyNames) {
      foreach (string propertyName in propertyNames) {
        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }
}
