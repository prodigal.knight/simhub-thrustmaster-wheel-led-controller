﻿using SimHub.ThrustmasterLEDControllerPlugin.Util;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace SimHub.ThrustmasterLEDControllerPlugin.Controls {
  public partial class NightDayDetection : UserControl, INotifyPropertyChanged {
    public static readonly DependencyProperty PluginProperty = DependencyProperty.Register(
      nameof(Plugin),
      typeof(ThrustmasterLEDControllerPlugin),
      typeof(NightDayDetection)
    );

    public ThrustmasterLEDControllerPlugin Plugin {
      get => (ThrustmasterLEDControllerPlugin) GetValue(PluginProperty);
      set => SetValue(PluginProperty, value);
    }

    public NightDayDetector NightDayDetector => this.Plugin?.NightDayDetector ?? new NightDayDetector(0, 0);
      
    public double Latitude {
      get => this.NightDayDetector.Latitude;
      set {
        this.NightDayDetector.Latitude = value;
        this.NotifyPropertyChanged("Sunrise", "Sunset", "TimeOfDay", "OnlineCurrentLocationDaylight");
      }
    }

    public double Longitude {
      get => this.NightDayDetector.Longitude;
      set {
        this.NightDayDetector.Longitude = value;
        this.NotifyPropertyChanged("Sunrise", "Sunset", "TimeOfDay", "OnlineCurrentLocationDaylight");
      }
    }

    public string SunAngle {
      get => this.NightDayDetector.SunAngle;
      set {
        this.NightDayDetector.SunAngle = value;
        this.NotifyPropertyChanged("Sunrise", "Sunset", "TimeOfDay");
      }
    }

    public string[] SunAngles => NightDayDetector.SunAngles;

    public string   Sunrise   => this.NightDayDetector.Sunrise;
    public string   Sunset    => this.NightDayDetector.Sunset;
    public string   TimeOfDay => string.Format(
      "{0} ({1})",
      this.NightDayDetector.IsNight() ? "Night" : "Day",
      DateTime.Now.ToLongTimeString()
    );

    public string OnlineCurrentLocationDaylight => string.Format(
      "https://www.timeanddate.com/sun/@{0},{1}",
      this.NightDayDetector.Latitude.ToString("0.000"),
      this.NightDayDetector.Longitude.ToString("0.000")
    );

    public event PropertyChangedEventHandler PropertyChanged;
    
    private readonly Poller   updateSunriseSunsetForDatePoller;
    private readonly Interval updateTimeOfDayInterval;

    public NightDayDetection() {      
      this.updateSunriseSunsetForDatePoller = new Poller(
        250,
        () => this.NightDayDetector.UpdateSunriseSunset(DateTime.Now),
        _  => this.NotifyPropertyChanged("Sunrise", "Sunset")
      );

      this.updateTimeOfDayInterval = new Interval(() => this.NotifyPropertyChanged("TimeOfDay"), 250);
      
      InitializeComponent();
    }
    
    /** Open a hyperlink in a web browser */
    private void OpenLink(object sender, RequestNavigateEventArgs e) {
      Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
      e.Handled = true;
    }

    /** Start pollers when the pane is expanded */
    public void OnExpanded(object sender, RoutedEventArgs e) {
      this.NotifyAllPropertiesChanged();

      this.updateSunriseSunsetForDatePoller.Start();
      this.updateTimeOfDayInterval.Start();
    }

    /** Stop pollers when the pane is collapsed */
    public void OnCollapsed(object sender, RoutedEventArgs e) {
      this.updateSunriseSunsetForDatePoller.Stop();
      this.updateTimeOfDayInterval.Stop();
    }

    public void NotifyAllPropertiesChanged() {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
    }

    private void NotifyPropertyChanged(params string[] propertyNames) {
      foreach (string propertyName in propertyNames) {
        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }
}
