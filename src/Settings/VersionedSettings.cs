﻿using Newtonsoft.Json.Linq;
using SimHub.ThrustmasterLEDControllerPlugin.Util;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  public abstract class VersionedSettings<T> : SettingsCommon<T> where T : SettingsCommon<T> {
    /**
     * Parse version string from settings file
     *
     * @param  {JObject} file Parsed JSON settings file
     *
     * @return {Version}
     */
    public static Version ParsePluginVersionFromFile(JObject file) {      
      return Version.Parse(VersionedSettings<T>.ReadValueFromRaw<string>(file, "pluginVersion", null));
    }

    public readonly string pluginVersion = ThrustmasterLEDControllerPlugin.PluginVersion;
  }
}
