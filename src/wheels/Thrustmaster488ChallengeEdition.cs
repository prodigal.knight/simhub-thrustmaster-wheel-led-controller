using HidLibrary;
using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimHub.ThrustmasterLEDControllerPlugin.Wheels {
  public class Thrustmaster488ChallengeEdition : Wheel {
    /** Utility function which returns a base instruction buffer for a given command */
    private static byte[] createInstructionBuffer(byte command) {
      const int byteLength = 64;
      byte[] buf = new byte[byteLength];

      buf[0] = 0x60; // Magic number. Meaning unknown.
      buf[1] = 0x00; // Magic number. Meaning unknown.
      buf[2] = 0x41; // Magic number. Meaning unknown.
      buf[3] = command;

      // Ensure remaining buffer data is blank (probably unnecessary but I started with C so I'm paranoid)
      for (int i = 4; i < byteLength; i++) {
        buf[i] = 0x00;
      }

      return buf;
    }
    
    public const    byte   numRevLEDs = 15;

    /** Human-readable wheel name */
    public override string Name => "Thrustmaster Ferrari 488 Challenge Edition";
    /** Human-unreadable settings name for wheel */
    public override string SettingsName => "tm_f488ce";

    private readonly LedAnimation[] animations = new List<LedAnimation>{
      LedAnimation.None,
      LedAnimation.crossingLeds(numRevLEDs),
      LedAnimation.alternatingLeds(numRevLEDs),
      LedAnimation.leftToRight(numRevLEDs),
      LedAnimation.rightToLeft(numRevLEDs),
      LedAnimation.blinkingLeds(numRevLEDs),
      LedAnimation.blinkingCenterLeds(numRevLEDs),
      LedAnimation.blinkingEndLeds(numRevLEDs),
      LedAnimation.explodingLeds(numRevLEDs),
    }
      .Concat(Enumerable.Range(0, numRevLEDs).Select(idx => LedAnimation.blinkSingleLed(idx)))
      .ToArray();

    // Animations this wheel can display
    protected override LedAnimation[]         availableAnimations => this.animations;

    // Shift lights techniques this wheel can use
    protected override ShiftLightsTechnique[] availableTechniques => new ShiftLightsTechnique[]{
      ShiftLightsTechnique.None,
      ShiftLightsTechnique.ltr(numRevLEDs),
      ShiftLightsTechnique.etc(numRevLEDs),
      ShiftLightsTechnique.cte(numRevLEDs),
      ShiftLightsTechnique.ltrThirds(numRevLEDs),
    };

    // Command buffers
    private readonly byte[] ledUpdateInstruction        = createInstructionBuffer(0x02);
    private readonly byte[] brightnessUpdateInstruction = createInstructionBuffer(0x10);

    private ushort currentLedState = 0x0000;

    public Thrustmaster488ChallengeEdition(HidDevice device) : base(device) {}

    /** Send a brightness update command to the wheel if different from current brightness */
    protected override void setBrightness(int brightness) {
      if (brightness == this.currentBrightness) {
        return;
      }

      this.currentBrightness = (byte) Math.Min(Math.Max(brightness, 0), 100);

      this.brightnessUpdateInstruction[4] = this.currentBrightness;

      this.device.Write(brightnessUpdateInstruction);
    }

    /** Send an LED state update command to the wheel if different from current LED state */
    protected override void setLedState(ushort ledState) {
      if (ledState == this.currentLedState) {
        return;
      }

      this.currentLedState = ledState;

      // convert little-endian to big-endian
      this.ledUpdateInstruction[4] = (byte) (this.currentLedState & 0xFF);
      this.ledUpdateInstruction[5] = (byte) (this.currentLedState >> 8);

      this.device.Write(ledUpdateInstruction);
    }
  }
}
