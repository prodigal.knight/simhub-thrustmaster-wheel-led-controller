﻿using System;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  internal class Poller : Poller<bool> {
    /**
     * @constructor
     *
     * @param  {int}          rate     Polling rate
     * @param  {Func<bool>}   poll          Perform polling task
     * @param  {Action<bool>} onResultFound Callback for when polling task determines a result
     */
    public Poller(
      int          rate,
      Func<bool>   poll,
      Action<bool> onResultFound
    ) : base(rate, poll, onResultFound) {}

    /**
     * @constructor
     *
     * @param  {int}                rate     Polling rate
     * @param  {Func<bool, Task>}   poll          Perform polling task
     * @param  {Action<bool, Task>} onResultFound Callback for when polling task determines a result
     */
    public Poller(
      int              rate,
      Func<bool>       poll,
      Func<bool, Task> onResultFound
    ) : base(rate, poll, onResultFound) {}

    /**
     * Check to make sure polling task returned a valid result
     *
     * @param  {bool} result The result returned by the polling task
     *
     * @return bool
     */
    protected override bool IsValidResult(bool result) {
      return result;
    }
  }

  internal class Poller<T> {
    public bool IsPolling => this.interval.Running;

    public int Rate {
      get => this.interval.Delay;
      set => this.interval.Delay = value;
    }

    private readonly Interval      interval;

    private readonly Func<T>       check;
    private readonly Func<T, Task> callback;
        
    private bool stopOnResultFound;

    /**
     * @constructor
     *
     * @param  {int}       rate     Polling rate
     * @param  {Func<T>}   check    Perform polling task
     * @param  {Action<T>} callback Callback for when polling task determines a result
     */
    public Poller(
      int       rate,
      Func<T>   check,
      Action<T> callback
    ) : this(rate, check, arg => Task.Run(() => callback(arg))) {}

    /**
     * @constructor
     *
     * @param  {int}             rate     Polling rate
     * @param  {Func<T, Task>}   check    Perform polling task
     * @param  {Action<T, Task>} callback Callback for when polling task determines a result
     */
    public Poller(
      int           rate,
      Func<T>       check,
      Func<T, Task> callback
    ) {
      this.check    = check;
      this.callback = callback;

      this.interval = new Interval(this.Run, rate);
    }
    /**
     * Start polling
     *
     * @param  {bool} stopOnResultFound Whether or not to stop when a result is found by the polling task
     */
    public void Start(bool stopOnResultFound = false) {
      this.stopOnResultFound = stopOnResultFound;

      this.interval.Start();
    }

    /** Stop polling */
    public void Stop() {
      this.interval.Stop();
    }

    /**
     * Check to make sure polling task returned a valid result
     *
     * @param  {bool} result The result returned by the polling task
     *
     * @return bool
     */
    protected virtual bool IsValidResult(T result) {
      return result != null;
    }

    /**
     * Polling orchestrator function
     *
     * @param  {bool} stopOnResultFound Whether or not to stop when a result is found by the polling task
     *
     * @return Task
     */
    private void Run() {
      T result = this.check();

      if (this.IsValidResult(result)) {
        _ = this.callback(result);

        if (this.stopOnResultFound) {
          this.interval.Stop();
        }
      }
    }
  }
}
