﻿using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Util;
using SimHub.ThrustmasterLEDControllerPlugin.Wheels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace SimHub.ThrustmasterLEDControllerPlugin.Controls {
  public partial class AnimationControl : UserControl, INotifyPropertyChanged {
    public static readonly DependencyProperty LabelTextProperty    = DependencyProperty.Register(
      nameof(LabelText),
      typeof(string),
      typeof(AnimationControl)
    );
    public static readonly DependencyProperty LabelTooltipProperty = DependencyProperty.Register(
      nameof(LabelTooltip),
      typeof(string),
      typeof(AnimationControl)
    );
    public static readonly DependencyProperty AnimationProperty    = DependencyProperty.Register(
      nameof(Animation),
      typeof(LedAnimation),
      typeof(AnimationControl)
    );
    public static readonly DependencyProperty AnimationKeyProperty = DependencyProperty.Register(
      nameof(AnimationKey),
      typeof(string),
      typeof(AnimationControl)
    );
    public static readonly DependencyProperty WheelProperty        = DependencyProperty.Register(
      nameof(Wheel),
      typeof(Wheel),
      typeof(AnimationControl)
    );
    public static readonly DependencyProperty PluginProperty       = DependencyProperty.Register(
      nameof(Plugin),
      typeof(ThrustmasterLEDControllerPlugin),
      typeof(AnimationControl)
    );

    public string LabelText {
      get => (string) GetValue(LabelTextProperty);
      set => SetValue(LabelTextProperty, value);
    }

    public string LabelTooltip {
      get => (string) GetValue(LabelTooltipProperty);
      set => SetValue(LabelTooltipProperty, value);
    }
    
    public LedAnimation Animation {
      get => (LedAnimation) GetValue(AnimationProperty);
      set => SetValue(AnimationProperty, value);
    }

    public string AnimationKey {
      get => (string) GetValue(AnimationKeyProperty);
      set => SetValue(AnimationKeyProperty, value);
    }
    
    public Wheel Wheel {
      get => (Wheel) GetValue(WheelProperty);
      set => SetValue(WheelProperty, value);
    }

    public ThrustmasterLEDControllerPlugin Plugin {
      get => (ThrustmasterLEDControllerPlugin) GetValue(PluginProperty);
      set => SetValue(PluginProperty, value);
    }

    public bool Enabled {
      get => this.BlinkRate != 0;
      set {
        if (value) {
          this.BlinkRate = 50;
        } else {
          this.BlinkRate = 0;
        }
        this.NotifyAllPropertiesChanged();
      }
    }

    public int BlinkRate {
      get => this.Animation?.BlinkRate ?? 50;
      set => this.Animation.BlinkRate = value;
    }

    public byte Brightness {
      get => this.Animation?.Brightness ?? 50;
      set {
        this.Animation.Brightness = value;
        this.Wheel.SetBrightness(value);
      }
    }

    public List<string> Animations => this.Wheel?.AvailableAnimations ?? new List<string>();

    public int AnimationIndex => Math.Max(this.Animations.FindIndex(animation => animation == this.Animation.name), 0);

    public bool Expanded { get; set; }
    
    public event PropertyChangedEventHandler PropertyChanged;

    // Function call debouncers for previews
    private readonly DebounceDispatcher brightnessDebouncer = new DebounceDispatcher(500);
    private readonly DebounceDispatcher blinkRateDebouncer  = new DebounceDispatcher(500);

    private bool hasLedLock = false;

    public AnimationControl() {
      InitializeComponent();
    }

    /** Preview the selected animation on change */
    public void OnAnimationChangedPreviewAnimation(object sender, SelectionChangedEventArgs e) {
      if (e.RemovedItems.Count != 0 && this.Expanded && this.Enabled) {
        this.Wheel.ClearAnimation();

        string animationName = e.AddedItems[0] as string;
        
        int blinkRate = this.BlinkRate;
        byte brightness = this.Brightness;

        LedAnimation animation = this.Wheel.SetAnimation(this.AnimationKey, animationName);
        // Have the new animation inherit the current blink rate and brightness settings
        animation.BlinkRate = blinkRate;
        animation.Brightness = brightness;

        this.Plugin.LedLock.RequestLockWhile(async () => {
          // Preview animation with current blink rate and brightness settings
          await this.Wheel.PlayAnimationOnceOrForDuration(animation, blinkRate, brightness);
        });
      }
    }

    /** Start previewing the current animation when adjusting blink rate */
    public void OnBlinkRateMouseLeftButtonDown(object sender, RoutedEventArgs e) {
      this.onMouseLeftButtonDownStartAnimation();
    }

    /** Stop previewing current animation when done adjusting blink rate */
    public void OnBlinkRateMouseLeftButtonUp(object sender, RoutedEventArgs e) {
      this.onMouseLeftButtonUpClearAnimation(this.blinkRateDebouncer);
    }

    /** Start previewing the current animation when adjusting brihtness */
    public void OnBrightnessMouseLeftButtonDown(object sender, RoutedEventArgs e) {
      this.onMouseLeftButtonDownStartAnimation();
    }

    /** Stop previewing current animation when done adjusting brihtness */
    public void OnBrightnessMouseLeftButtonUp(object sender, RoutedEventArgs e) {
      this.onMouseLeftButtonUpClearAnimation(this.brightnessDebouncer);
    }

    /** Start playing the currently selected animation */
    private async void onMouseLeftButtonDownStartAnimation() {
      if (!this.hasLedLock) {
        await this.Plugin.LedLock.RequestLock();
        this.hasLedLock = true;

        this.Wheel.StartAnimation(this.Animation);
      }
    }

    /** Stop playing the currently selected animation */
    private void onMouseLeftButtonUpClearAnimation(DebounceDispatcher debouncer) {
      Action callback = () => {
        this.Wheel.ClearAnimation();
        _ = this.Wheel.TurnOffLEDs();

        this.hasLedLock = false;
        this.Plugin.LedLock.Unlock();
      };

      debouncer.Debounce(() => Dispatcher.BeginInvoke(callback));
    }

    public void NotifyAllPropertiesChanged() {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
    }

    private void NotifyPropertyChanged(params string[] propertyNames) {
      foreach (string propertyName in propertyNames) {
        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }
}
