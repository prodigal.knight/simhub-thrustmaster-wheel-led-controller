﻿using Newtonsoft.Json.Linq;
using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Util;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  public class AnimationSettings : SettingsCommon<AnimationSettings> {
    /** Migrate from an older plugin version's settings to the current plugin version's settings without losing data */
    public static AnimationSettings UpgradeFromLegacySettings(Version version, JObject raw, string name) {
      AnimationSettings settings = new AnimationSettings();

      // NOTE: Plugin settings file didn't track version before 1.2.0
      if (version >= "1.2.0") {
        return AnimationSettings.ReadValueFromRaw(raw, name, settings);
      }

      settings.name       = AnimationSettings.ReadValueFromRaw(raw, name,               settings.name);
      settings.blinkRate  = AnimationSettings.ReadValueFromRaw(raw, name + "BlinkRate", settings.blinkRate);
      // NOTE: Assume whatever the plugin's main brightness setting was for each individual animation
      settings.brightness = AnimationSettings.ReadValueFromRaw(raw, "brightness",       settings.brightness);

      return settings;
    }

    public string name       = "none";
    public int    blinkRate  = 50;
    public byte   brightness = 50;

    public AnimationSettings() {}

    public AnimationSettings(LedAnimation animation) : this(
      animation.name,
      animation.BlinkRate,
      animation.Brightness
    ) {}

    public AnimationSettings(string name, int blinkRate, byte brightness) {
      this.name       = name;
      this.blinkRate  = blinkRate;
      this.brightness = brightness;
    }

    public override AnimationSettings Clone() {
      return new AnimationSettings {
        name       = this.name,
        blinkRate  = this.blinkRate,
        brightness = this.brightness,
      };
    }
  }
}
