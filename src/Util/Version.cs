﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  public class Version {
    private static readonly Version EMPTY_VERSION = new Version("0.0.0");

    // NOTE: Limit memory usage by reusing cached objects
    private static readonly ConditionalWeakTable<string, Version> cache = new ConditionalWeakTable<string, Version>();

    public static bool operator < (Version a, Version b) {
      return (
        a.major < b.major ||
        (a.major == b.major && a.minor < b.minor) ||
        (a.major == b.major && a.minor == b.minor && a.patch < b.patch) ||
        (a.major == b.major && a.minor == b.minor && a.patch == b.patch && a.build < b.build)
      );
    }
    public static bool operator > (Version a, Version b) {
      return (
        a.major > b.major ||
        (a.major == b.major && a.minor > b.minor) ||
        (a.major == b.major && a.minor == b.minor && a.patch > b.patch) ||
        (a.major == b.major && a.minor == b.minor && a.patch == b.patch && a.build > b.build)
      );
    }

    public static bool operator <= (Version a, Version b) {
      return a.major <= b.major && a.minor <= b.minor && a.patch <= b.patch && a.build <= b.build;
    }
    public static bool operator >= (Version a, Version b) {
      return a.major >= b.major && a.minor >= b.minor && a.patch >= b.patch && a.build >= b.build;
    }
    
    // NOTE: String comparison utilities for convenience
    public static bool operator == (Version a, string b) {
      return a == Version.Parse(b);
    }
    public static bool operator != (Version a, string b) {
      return a != Version.Parse(b);
    }
    
    public static bool operator < (Version a, string b) {
      return a < Version.Parse(b);
    }
    public static bool operator > (Version a, string b) {
      return a > Version.Parse(b);
    }

    public static bool operator <= (Version a, string b) {
      return a <= Version.Parse(b);
    }
    public static bool operator >= (Version a, string b) {
      return a >= Version.Parse(b);
    }

    /**
     * Parse the given version string
     *
     * @param  {string} version
     *
     * @return Version
     */
    public static Version Parse(string version) {
      return version == null
        ? Version.EMPTY_VERSION
        : Version.cache.GetValue(version, _ => new Version(version))
        ;
    }

    private static int parseVersionPart(string versionPart) {
      if (Int32.TryParse(versionPart, out int part)) {
        return Math.Max(part, 0);
      }

      return 0;
    }

    private readonly int major;
    private readonly int minor;
    private readonly int patch;
    private readonly int build;

    private Version(string version) {
      string[] parts = version.Split('.', '-'); // NOTE: Ignore any suffixes like -alpha, -beta, etc.

      this.major = Version.parseVersionPart(parts.ElementAtOrDefault(0));
      this.minor = Version.parseVersionPart(parts.ElementAtOrDefault(1));
      this.patch = Version.parseVersionPart(parts.ElementAtOrDefault(2));
      this.build = Version.parseVersionPart(parts.ElementAtOrDefault(3));
    }

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }

    public override int GetHashCode() {
      return base.GetHashCode();
    }
  }
}
