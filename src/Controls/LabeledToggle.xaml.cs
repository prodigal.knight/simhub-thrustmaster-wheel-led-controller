﻿using System.Windows;
using System.Windows.Controls;

namespace SimHub.ThrustmasterLEDControllerPlugin.Controls {
  public partial class LabeledToggle : UserControl {
    public static readonly DependencyProperty LabelProperty           = DependencyProperty.Register(
      nameof(Label),
      typeof(string),
      typeof(LabeledToggle)
    );

    public static readonly DependencyProperty TooltipProperty         = DependencyProperty.Register(
      nameof(Tooltip),
      typeof(string),
      typeof(LabeledToggle)
    );

    public static readonly DependencyProperty IsCheckedProperty       = DependencyProperty.Register(
      nameof(IsChecked),
      typeof(bool),
      typeof(LabeledToggle)
    );

    public static readonly DependencyProperty OnToggleChangedProperty = DependencyProperty.Register(
      nameof(OnToggleChanged),
      typeof(RoutedEventHandler),
      typeof(LabeledToggle),
      new PropertyMetadata(null)
    );
    
    public string Label {
      get => (string) GetValue(LabelProperty);
      set => SetValue(LabelProperty, value);
    }

    public string Tooltip {
      get => (string) GetValue(TooltipProperty);
      set => SetValue(TooltipProperty, value);
    }

    public bool IsChecked {
      get => (bool) GetValue(IsCheckedProperty);
      set => SetValue(IsCheckedProperty, value);
    }

    public RoutedEventHandler OnToggleChanged {
      get => (RoutedEventHandler) GetValue(OnToggleChangedProperty);
      set => SetValue(OnToggleChangedProperty, value);
    }
    
    public LabeledToggle() {
      InitializeComponent();
    }

    private void InternalOnToggleChanged(object sender, RoutedEventArgs e) {
      this.OnToggleChanged?.Invoke(sender, e);
    }
  }
}
