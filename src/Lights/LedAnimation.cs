﻿using SimHub.ThrustmasterLEDControllerPlugin.Util;
using System;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Lights {
  public class LedAnimation {
    /**
     * Animation which blinks alternating LEDs, e.g.: For a 5-LED wheel, where 0 is off and X is on
     *
     * X0X0X
     * 0X0X0
     */
    public static LedAnimation alternatingLeds(int numLeds) {
      ushort baseFrame = 0x0000;
      ushort xorMask = 0x0000;

      for (int i = 0; i < numLeds; i++) {
        if (i % 2 == 0) {
          baseFrame |= (ushort)(1 << i);
        }
        xorMask |= (ushort)(1 << i);
      }

      return new LedAnimation("alternating", new ushort[] { baseFrame, (ushort)(baseFrame ^ xorMask) });
    }

    /** Animation which blinks all LEDs */
    public static LedAnimation blinkingLeds(int numLeds) {
      ushort baseFrame = 0x0000;

      for (int i = 0; i < numLeds; i++) {
        baseFrame |= (ushort)(1 << i);
      }

      return new LedAnimation("blinking", new ushort[] { 0x0000, baseFrame });
    }

    /** Animation which blinks only the outer LEDs (defaults to left/right 1/3) */
    public static LedAnimation blinkingEndLeds(int numLeds, int numEndLedsToBlink = 0) {
      if (numEndLedsToBlink == 0) {
        numEndLedsToBlink = numLeds / 3;
      }

      ushort baseFrame = 0x0000;

      for (int i = 0; i < numLeds; i++) {
        if (i < numEndLedsToBlink || i >= numLeds - numEndLedsToBlink) {
          baseFrame |= (ushort)(1 << i);
        }
      }

      return new LedAnimation("blinkingEnds", new ushort[] { 0x0000, baseFrame });
    }

    /** Animation which blinks only the center LEDs (defaults to center 1/3) */
    public static LedAnimation blinkingCenterLeds(int numLeds, int numEndLedsToIgnore = 0) {
      if (numEndLedsToIgnore == 0) {
        numEndLedsToIgnore = numLeds / 3;
      }

      ushort baseFrame = 0x0000;

      for (int i = 0; i < numLeds; i++) {
        if (i >= numEndLedsToIgnore && i < numLeds - numEndLedsToIgnore) {
          baseFrame |= (ushort)(1 << i);
        }
      }

      return new LedAnimation("blinkingCenter", new ushort[] { 0x0000, baseFrame });
    }

    /** Animation where LEDs go from left side to right side of wheel */
    public static LedAnimation leftToRight(int numLeds) {
      ushort[] frames = new ushort[numLeds];

      for (int i = 0; i < numLeds; i++) {
        frames[i] = (ushort)(1 << i);
      }

      return new LedAnimation("leftToRight", frames);
    }

    /** Animation where LEDs go from right side to left side of wheel */
    public static LedAnimation rightToLeft(int numLeds) {
      ushort[] frames = new ushort[numLeds];

      for (int i = 0, j = numLeds - 1; i < numLeds; i++, j--) {
        frames[i] = (ushort)(1 << j);
      }

      return new LedAnimation("rightToLeft", frames);
    }

    /** Animation with LEDs starting from edges and crossing through the middle to the opposite edge */
    public static LedAnimation crossingLeds(int numLeds) {
      ushort[] frames = new ushort[numLeds];

      for (int i = 0, j = numLeds - 1; i < numLeds; i++, j--) {
        frames[i] = (ushort)(1 << i | 1 << j);
      }

      return new LedAnimation("crossing", frames);
    }

    /**
     * Animation with an "exploding" effect starting from the middle, going to the edges, and receding back to the
     * middle
     */
    public static LedAnimation explodingLeds(int numLeds) {
      ushort[] frames = new ushort[numLeds];

      int middle = numLeds / 2 + 1;

      for (int i = middle, j = 0, k = numLeds - 1; j < numLeds; i++, j++, k--) {
        frames[i % numLeds] = (ushort)(1 << j | 1 << k);
      }

      return new LedAnimation("exploding", frames);
    }

    /** Animation which blinks a single LED */
    public static LedAnimation blinkSingleLed(int ledIdx) {
      return new LedAnimation(
        string.Format("blink {0} led", Conversions.ToOrdinal(ledIdx + 1)),
        new ushort[] {0x0000, (ushort)(0x00001 << ledIdx)}
      );
    }

    public static LedAnimation None = new LedAnimation("none", new ushort[] { });

    public bool Playing => this.playing;

    public int  BlinkRate { get; set; }

    public byte Brightness { get; set; }

    public  readonly string   name;

    private readonly ushort[] frames;
    private readonly int      numFrames;

    private bool playing    = false;
    private bool shouldPlay = false;
    private bool paused     = false;

    /**
     * @constructor
     *
     * @param {string}   name   The name of the animation
     * @param {ushort[]} frames The individual frames this animation is composed of
     */
    private LedAnimation(string name, ushort[] frames) {
      this.name      = name;
      this.frames    = frames;
      this.numFrames = frames.Length;
    }

    /**
     * Plays animation until manually stopped
     *
     * @param  {int}            frameDurationMs Delay between frames
     * @param  {Action<ushort>} activateLeds    Callback which sets LED state on the wheel
     *
     * @return Task<bool>
     */
    public Task<bool> play(Action<ushort> activateLeds) {
      // Essentially infinite as n has to wrap around the whole range of a 32-bit integer
      return playNTimes(uint.MaxValue, activateLeds);
    }

    /**
     * Plays animation once or until manually stopped
     *
     * @param  {int}            frameDurationMs Delay between frames
     * @param  {Action<ushort>} activateLeds    Callback which sets LED state on the wheel
     *
     * @return Task<bool>
     */
    public Task<bool> playOnce(Action<ushort> activateLeds) {
      return playNTimes(1, activateLeds);
    }

    /**
     * Plays animation N times or until manually stopped
     *
     * @param  {uint}           n               Number of times to play the animation
     * @param  {int}            frameDurationMs Delay between frames
     * @param  {Action<ushort>} activateLeds    Callback which sets LED state on the wheel
     *
     * @return Task<bool>
     */
    public Task<bool> playNTimes(uint n, Action<ushort> activateLeds) {
      return playNTimesOrMinDuration(0, n, activateLeds);
    }

    /**
     * Plays animation N times or for a minimum duration or until manually stopped
     *
     * @param  {int}            minDurationMs   Minimum duration for which to continue playing the animation
     * @param  {uint}           n               Number of times to play the animation
     * @param  {int}            frameDurationMs Delay between frames
     * @param  {Action<ushort>} activateLeds    Callback which sets LED state on the wheel
     *
     * @return Task<bool>
     */
    public async Task<bool> playNTimesOrMinDuration(int minDurationMs, uint n, Action<ushort> activateLeds) {
      shouldPlay = true;
      paused = false;

      if (BlinkRate == 0 || numFrames == 0 || playing) {
        return false;
      }

      long start = DateTimeOffset.Now.ToUnixTimeMilliseconds();

      int frameIdx = 0;
      int iteration = 0;

      playing = true;
      while (shouldPlay) {
        if (!paused) {
          activateLeds(frames[frameIdx]);

          frameIdx = (frameIdx + 1) % numFrames;
          if (frameIdx == 0) {
            if (BlinkRate == 0) {
              shouldPlay = false;
            } else if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - start >= minDurationMs) {
              shouldPlay &= ++iteration < n;
            }
          }
        }

        await Task.Delay(BlinkRate);
      }
      playing = false;

      return true;
    }

    /** Pause this animation (if playing) to be resumed at a later time */
    public void pause() {
      paused = true;
    }

    /** Resume playing this animation if paused */
    public void resume() {
      paused = false;
    }

    /** Stop playing this animation (if playing) */
    public void stop() {
      shouldPlay = false;
    }

    public LedAnimation Clone() {
      return new LedAnimation(name, frames) {
        BlinkRate  = this.BlinkRate,
        Brightness = this.Brightness
      };
    }
  }
}
