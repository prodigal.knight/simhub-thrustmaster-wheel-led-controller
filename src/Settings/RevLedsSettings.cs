﻿using Newtonsoft.Json.Linq;
using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Util;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  public class RevLedsSettings : SettingsCommon<RevLedsSettings> {
    /** Migrate from an older plugin version's settings to the current plugin version's settings without losing data */
    public static RevLedsSettings UpgradeFromLegacySettings(Version version, JObject raw) {
      RevLedsSettings settings = new RevLedsSettings();
      
      // NOTE: Plugin settings file didn't track version before 1.2.0
      if (version >= "1.2.0") {
        return RevLedsSettings.ReadValueFromRaw(raw, "revLeds", settings);
      }

      settings.brightness = RevLedsSettings.ReadValueFromRaw(raw, "brightness",       settings.brightness);
      settings.minRpmPct  = RevLedsSettings.ReadValueFromRaw(raw, "minRpmPercentage", settings.minRpmPct);
      settings.maxRpmPct  = RevLedsSettings.ReadValueFromRaw(raw, "maxRpmPercentage", settings.maxRpmPct);
      settings.nightMode  = RevLedsSettings.ReadValueFromRaw(raw, "nightMode",        settings.nightMode);
      settings.rpmScaling = RevLedsSettings.ReadValueFromRaw(raw, "rpmScalingMethod", settings.rpmScaling);

      return settings;
    }
    
    public NightMode nightMode  = NightMode.Off;
    public byte      brightness = 50;
    public double    minRpmPct  = 0.5;
    public double    maxRpmPct  = 1.0;
    public string    rpmScaling = "linear";

    public RevLedsSettings() {}
    
    public RevLedsSettings(RevLeds revLeds) : this(
      revLeds.Brightness,
      revLeds.MinRpmPct,
      revLeds.MaxRpmPct,
      revLeds.NightMode,
      revLeds.ScalingMethod
    ) {}

    public RevLedsSettings(
      byte      brightness,
      double    minRpmPct,
      double    maxRpmPct,
      NightMode nightMode,
      string    rpmScaling
    ) {
      this.brightness = brightness;
      this.minRpmPct  = minRpmPct;
      this.maxRpmPct  = maxRpmPct;
      this.nightMode  = nightMode;
      this.rpmScaling = rpmScaling;
    }

    public override RevLedsSettings Clone() {
      return new RevLedsSettings {
        brightness = this.brightness,
        minRpmPct  = this.minRpmPct,
        maxRpmPct  = this.maxRpmPct,
        nightMode  = this.nightMode,
        rpmScaling = this.rpmScaling,
      };
    }
  }
}
