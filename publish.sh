#!/bin/bash
set -e

if [ -z "$1" ]; then
  echo "No version specified"

  exit 1
fi

version="$1"

publish_token="$(cat .publish-token)"

if [ -z "$publish_token" ]; then
  echo "No publish token found in filesystem, unable to publish package"

  exit 2
fi

dllFile="./bin/Release/SimHub.ThrustmasterLEDControllerPlugin.dll"

if [ ! -e "$dllFile" ]; then
  echo "${dllFile} does not exist!"

  exit 3
fi

zip -rj release.zip "${dllFile}"

echo "Publishing v${version} to gitlab"

curl --header "Authorization: Bearer ${publish_token}" \
     --upload-file release.zip \
     "https://gitlab.com/api/v4/projects/43240488/packages/generic/simhub-thrustmaster-led-controller-plugin/${version}/release.zip"
