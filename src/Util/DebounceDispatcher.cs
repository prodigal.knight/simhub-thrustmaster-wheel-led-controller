﻿using System;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  // DebounceDispatcher class adapted from https://github.com/coddicat/DebounceThrottle/blob/master/DebounceThrottle/DebounceDispatcher.cs
  public class DebounceDispatcher : DebounceDispatcher<bool> {
    /**
     * @constructor
     *
     * @param {int} delay Minimum delay to enforce before calling debounced function
     */
    public DebounceDispatcher(int delay) : base(delay) {}

    /** Debounce the given callback function */
    public Task DebounceAsync(Func<Task> callback) {
      return base.DebounceAsync(async () => {
        await callback.Invoke();
        return true;
      });
    }

    /** Debounce the given callback function */
    public void Debounce(Action callback) {
      base.Debounce(() => {
        callback.Invoke();
        return true;
      });
    }
  }

  // NOTE: Removed some unnecessary state tracking from the original library
  public class DebounceDispatcher<T> {
    private DateTime      lastInvokeTime;
    private int           delay;
    private Func<Task<T>> callback;
    private Task<T>       waitingTask;

    public int Delay {
      get => this.delay;
      set => this.delay = value;
    }

    /**
     * @constructor
     *
     * @param {int} delay Minimum delay to enforce before calling debounced function
     */
    public DebounceDispatcher(int delay) {
      this.delay = delay;
    }

    /** Debounce the given callback function */
    public Task<T> DebounceAsync(Func<Task<T>> callback) {
      lastInvokeTime = DateTime.UtcNow;

      if (waitingTask != null) {
        return waitingTask;
      }

      this.callback = callback;

      waitingTask = Task.Run(async () => {
        do {
          int nextDelay = (int)(this.delay - (DateTime.UtcNow - lastInvokeTime).TotalMilliseconds);

          await Task.Delay(nextDelay);
        } while ((DateTime.UtcNow - lastInvokeTime).TotalMilliseconds < delay);

        try {
          return this.callback.Invoke().Result;
        } catch (Exception e) {
          Logger.error(e);
          throw;
        } finally {
          waitingTask = null;
        }
      });

      return waitingTask;
    }

    /** Debounce the given callback function */
    public Task<T> Debounce(Func<T> callback) {
      return this.DebounceAsync(() => Task.Run(() => callback()));
    }
  }
}
