﻿using Newtonsoft.Json.Linq;
using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Util;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  public class WheelSettings : VersionedSettings<WheelSettings> {
    /** Migrate from an older plugin version's settings to the current plugin version's settings without losing data */
    public static WheelSettings Migrate(JObject fileSettings, WheelSettings defaultSettings = null) {
      WheelSettings settings = defaultSettings?.Clone() ?? new WheelSettings();

      if (fileSettings == null) {
        return settings;
      }

      Version filePluginVersion  = WheelSettings.ParsePluginVersionFromFile(fileSettings);

      settings.shiftLightsMethod = WheelSettings.ReadValueFromRaw(
        fileSettings,
        "shiftLightsMethod",
        settings.shiftLightsMethod
      );

      settings.revLeds           = RevLedsSettings.UpgradeFromLegacySettings(filePluginVersion, fileSettings);

      settings.redLineAnimation  = AnimationSettings.UpgradeFromLegacySettings(
        filePluginVersion,
        fileSettings,
        // NOTE: Plugin version 1.2.0 changed the name from shiftPointAnimation to redLineAnimation
        filePluginVersion >= "1.2.0" ? "redLineAnimation" : "shiftPointAnimation"
      );
      settings.pitLaneAnimation  = AnimationSettings.UpgradeFromLegacySettings(
        filePluginVersion,
        fileSettings,
        "pitLaneAnimation"
      );
      settings.startupAnimation  = AnimationSettings.UpgradeFromLegacySettings(
        filePluginVersion,
        fileSettings,
        "startupAnimation"
      );
      settings.neutralAnimation  = AnimationSettings.UpgradeFromLegacySettings(
        filePluginVersion,
        fileSettings,
        "neutralAnimation"
      );
      settings.reverseAnimation  = AnimationSettings.UpgradeFromLegacySettings(
        filePluginVersion,
        fileSettings,
        "reverseAnimation"
      );

      return settings;
    }

    public string            shiftLightsMethod = "leftToRight";

    public RevLedsSettings   revLeds           = new RevLedsSettings(50, 0.5, 1.0, NightMode.Off, "linear");

    public AnimationSettings redLineAnimation  = new AnimationSettings("blinking",    50, 50);
    public AnimationSettings pitLaneAnimation  = new AnimationSettings("alternating", 50, 50);
    public AnimationSettings startupAnimation  = new AnimationSettings("crossing",    50, 50);
    public AnimationSettings neutralAnimation  = new AnimationSettings("none",        50, 50);
    public AnimationSettings reverseAnimation  = new AnimationSettings("none",        50, 50);

    public WheelSettings() {}

    public WheelSettings(
      string       shiftLightsMethod,

      RevLeds      revLeds,
      LedAnimation redLineAnimation,
      LedAnimation pitLaneAnimation,
      LedAnimation startupAnimation,
      LedAnimation neutralAnimation,
      LedAnimation reverseAnimation
    ) {
      this.shiftLightsMethod = shiftLightsMethod;

      this.revLeds           = new RevLedsSettings(revLeds);
      this.redLineAnimation  = new AnimationSettings(redLineAnimation);
      this.pitLaneAnimation  = new AnimationSettings(pitLaneAnimation);
      this.startupAnimation  = new AnimationSettings(startupAnimation);
      this.neutralAnimation  = new AnimationSettings(neutralAnimation);
      this.reverseAnimation  = new AnimationSettings(reverseAnimation);
    }

    public override WheelSettings Clone() {
      return new WheelSettings {
        shiftLightsMethod = this.shiftLightsMethod,

        revLeds           = this.revLeds.Clone(),

        redLineAnimation  = this.redLineAnimation.Clone(),
        pitLaneAnimation  = this.pitLaneAnimation.Clone(),
        startupAnimation  = this.startupAnimation.Clone(),
        neutralAnimation  = this.neutralAnimation.Clone(),
        reverseAnimation  = this.reverseAnimation.Clone()
      };
    }
  }
}
