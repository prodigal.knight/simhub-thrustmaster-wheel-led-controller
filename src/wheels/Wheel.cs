using GameReaderCommon;
using HidLibrary;
using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using SimHub.ThrustmasterLEDControllerPlugin.Settings;
using SimHub.ThrustmasterLEDControllerPlugin.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Wheels {
  public abstract class Wheel {
/******************************************************************************\
 *          ABSTRACT MEMBERS TO BE IMPLEMENTED BY INDIVIDUAL WHEELS           *
\******************************************************************************/
    /** Human-readable wheel name */
    public abstract string Name         { get; }
    /** Human-unreadable settings name for wheel */
    public abstract string SettingsName { get; }

    /** The animations this wheel supports */
    protected abstract LedAnimation[]         availableAnimations { get; }
    /** The shift lights techniques this wheel supports */
    protected abstract ShiftLightsTechnique[] availableTechniques { get; }
    
    /** Send a brightness update command to the wheel if different from current brightness */
    protected abstract void setBrightness(int brightness);

    /** Send an LED state update command to the wheel if different from current LED state */
    protected abstract void setLedState(ushort ledState);
    
/******************************************************************************\
 *                             COMMON WHEEL LOGIC                             *
\******************************************************************************/
    /** The names of the various animations this wheel can use */
    public List<string> AvailableAnimations           => this.availableAnimations
      .Select(animation => animation.name)
      .ToList();
    /** The names of the various shift light techniques this wheel can use */
    public List<string> AvailableShiftLightTechniques => this.availableTechniques
      .Select(technique => technique.name)
      .ToList();

    public RevLeds RevLeds => this.revLeds;
    
    /** The name of the current shift light technique */
    public string ShiftLightTechnique {
      get => this.currentTechnique.name;
      set { if (!string.IsNullOrEmpty(value)) { this.currentTechnique = this.getTechnique(value); } }
    }
    /** The name of the current startup animation */
    public LedAnimation StartupAnimation {
      get => this.startupAnimation;
      set { if (value != null) { this.startupAnimation = this.getAnimation(value.name); } }
    }
    /** The name of the current pit lane animation */
    public LedAnimation PitLaneAnimation {
      get => this.pitLaneAnimation;
      set { if (value != null) { this.pitLaneAnimation = this.getAnimation(value.name); } }
    }
    /** The name of the current shift point animation */
    public LedAnimation RedLineAnimation {
      get => this.redLineAnimation;
      set { if (value != null) { this.redLineAnimation = this.getAnimation(value.name); } }
    }
    /** The name of the current neutral animation */
    public LedAnimation NeutralAnimation {
      get => this.neutralAnimation;
      set { if (value != null) { this.neutralAnimation = this.getAnimation(value.name); } }
    }
    /** The name of the current reverse animation */
    public LedAnimation ReverseAnimation {
      get => this.reverseAnimation;
      set { if (value != null) { this.reverseAnimation = this.getAnimation(value.name); } }
    }

    public LedAnimation CurrentAnimation => this.currentAnimation;
    
    // Device handle
    protected readonly HidDevice device;

    // Current LED brightness
    protected byte currentBrightness = 50;
    protected ShiftLightsTechnique currentTechnique;
    protected readonly RevLeds revLeds;

    protected LedAnimation redLineAnimation = LedAnimation.None;
    protected LedAnimation pitLaneAnimation = LedAnimation.None;
    protected LedAnimation startupAnimation = LedAnimation.None;
    protected LedAnimation neutralAnimation = LedAnimation.None;
    protected LedAnimation reverseAnimation = LedAnimation.None;

    // The animation that's currently running (if any)
    private LedAnimation currentAnimation = LedAnimation.None;

    // Used to stop shift lights technique preview if it is currently running
    private CancellationTokenSource techniquePreview;

    public WheelSettings CurrentSettings {
      /** Return the wheel's current settings so they can be saved to a file */
      get => new WheelSettings(
        this.ShiftLightTechnique,
        this.revLeds,
        this.redLineAnimation,
        this.pitLaneAnimation,
        this.startupAnimation,
        this.neutralAnimation,
        this.reverseAnimation
      );
      /** Set current settings from file */
      set {
        this.revLeds.Init(value.revLeds);

        this.currentTechnique = this.getTechnique(value.shiftLightsMethod);

        this.initializeAnimation(ref this.startupAnimation, value.startupAnimation);
        this.initializeAnimation(ref this.redLineAnimation, value.redLineAnimation);
        this.initializeAnimation(ref this.pitLaneAnimation, value.pitLaneAnimation);
        this.initializeAnimation(ref this.neutralAnimation, value.neutralAnimation);
        this.initializeAnimation(ref this.reverseAnimation, value.reverseAnimation);
      }
    }

    public Wheel(HidDevice device) {
      this.device = device;
      this.revLeds = new RevLeds(this.setLedState);
    }
    
    /** Initialize wheel settings and play startup animation */
    public async Task<bool> Init(WheelSettings settings) {
      this.CurrentSettings = settings;

      // Open a communications channel with the device
      this.device?.OpenDevice();

      Logger.info("Playing startup animation");
      await this.setCurrentAnimation(startupAnimation).playNTimes(2, this.setLedState);
      this.currentAnimation = LedAnimation.None;

      this.setBrightness(this.revLeds.Brightness);

      Logger.info("Finished playing startup animation. Turning off LEDs");
      await this.TurnOffLEDs();

      return true;
    }

    /** Cleanup function used to deactivate LEDs, disconnect from device handle */
    public async Task<bool> Deactivate() {
      this.CancelCurrentShiftLightsTechniquePreview();
      this.ClearAnimation();
      await this.TurnOffLEDs();
      this.device?.CloseDevice();
      this.device?.Dispose();

      return true;
    }
    
    /** Process a game data update and illuminate LEDs as per configuration */
    public Task<bool> ProcessGameData(
      StatusDataBase data,
      NightDayDetector nightDayDetector
    ) {
      return Task.Run(() => {
        double rpmPct = data.Rpms / data.CarSettings_RedLineRPM;

        LedAnimation animationToPlay = this.getAnimationToPlay(data, rpmPct);

        // If we're supposed to be playing an animation, make sure it's been started
        if (animationToPlay != LedAnimation.None) {
          if (!animationToPlay.Playing) {
            this.StartAnimation(animationToPlay);
          }

          return true;
        } else if (this.currentAnimation != LedAnimation.None) {
          // If we're not supposed to be playing an animation, clear the current one (if any)
          this.ClearAnimation();
        }

        // If we've made it past all the animation checks, display the correct # of LEDs for current
        // RPM percentage
        this.revLeds.display(rpmPct, nightDayDetector, this.currentTechnique);

        return true;
      });
    }

    /** Turn off all LEDs */
    public Task<bool> TurnOffLEDs() {
      return Task.Run(() => {
        this.setLedState((ushort) 0x0000);

        return true;
      });
    }

    /** Turn on all LEDs */
    public Task<bool> TurnOnLEDs() {
      return Task.Run(() => {
        this.setLedState((ushort) 0xFFFF);

        return true;
      });
    }

    /** Start the animation with the given name */
    public void StartAnimation(string animationName, int? blinkRate = null, byte? brightness = null) {
      this.setCurrentAnimation(animationName, blinkRate, brightness).play(this.setLedState);
    }

    public void StartAnimation(LedAnimation animation, int? blinkRate = null, byte? brightness = null) {
      this.setCurrentAnimation(animation, blinkRate, brightness).play(this.setLedState);
    }

    /** Play the animation with the given name once */
    public Task<bool> PlayAnimationOnce(string animationName, int? blinkRate = null, byte? brightness = null) {
      return this.PlayAnimationOnceOrForDuration(animationName, blinkRate, brightness);
    }
    public Task<bool> PlayAnimationOnce(LedAnimation animation, int? blinkRate = null, byte? brightness = null) {
      return this.PlayAnimationOnceOrForDuration(animation, blinkRate, brightness);
    }
    /** Play the animation with the given name once, or for a minimum duration */
    public Task<bool> PlayAnimationOnceOrForDuration(
      string animationName,
      int?   blinkRate     = null,
      byte?  brightness    = null
    ) {
      return this.PlayAnimationOnceOrForDuration(animationName, 500, blinkRate, brightness);
    }
    public Task<bool> PlayAnimationOnceOrForDuration(
      LedAnimation animation,
      int?         blinkRate  = null,
      byte?        brightness = null
    ) {
      return this.PlayAnimationOnceOrForDuration(animation, 500, blinkRate, brightness);
    }

    public async Task<bool> PlayAnimationOnceOrForDuration(
      string animationName,
      int    durationMs,
      int?   blinkRate     = null,
      byte?  brightness    = null
    ) {
      LedAnimation playingAnimation = this.currentAnimation;
      await this
        .setCurrentAnimation(animationName, blinkRate, brightness)
        .playNTimesOrMinDuration(durationMs, 1, this.setLedState);
      this.currentAnimation = playingAnimation;

      return true;
    }
    public async Task<bool> PlayAnimationOnceOrForDuration(
      LedAnimation animation,
      int          durationMs,
      int?         blinkRate  = null,
      byte?        brightness = null
    ) {
      LedAnimation playingAnimation = this.currentAnimation;
      await this
        .setCurrentAnimation(animation, blinkRate, brightness)
        .playNTimesOrMinDuration(durationMs, 1, this.setLedState);
      this.currentAnimation = playingAnimation;

      return true;
    }

    /** Stop playing the current animation, if any */
    public void StopAnimation() {
      this.currentAnimation.stop();
    }

    /** Pause the current animation, if any */
    public void PauseAnimation() {
      this.currentAnimation.pause();
    }
    
    /** Resume the current animation, if any */
    public void ResumeAnimation() {
      this.currentAnimation.resume();
    }

    /** Stop the current animation and reset to LedAnimation.None */
    public void ClearAnimation() {
      this.currentAnimation.stop();
      this.currentAnimation = LedAnimation.None;
      // Reset to rev led brightness
      this.setBrightness(this.revLeds.Brightness);
    }
    
    /** Play a preview of the shift lights technique with the given name for a given duration */
    public async Task<bool> PreviewShiftLightsTechnique(string techniqueName, NightDayDetector nightDayDetector) {
      this.currentAnimation?.pause();

      int frameDelayMs = (int) Math.Round((double) (1000 / 60)); // Play at ~60fps

      this.setBrightness(this.revLeds.Brightness);

      this.techniquePreview = new CancellationTokenSource();

      try {
        await this.revLeds.Preview(
          this.getTechnique(techniqueName),
          frameDelayMs,
          nightDayDetector,
          this.techniquePreview.Token
        );

        await this.TurnOffLEDs();
      } catch {
        // Do nothing, preview was cancelled by user
      } finally {
        this.techniquePreview = null;

        this.currentAnimation?.resume();
      }

      return true;
    }
    
    /** Stop current shift lights technique preview, if any */
    public void CancelCurrentShiftLightsTechniquePreview() {
      this.techniquePreview?.Cancel();
    }

    /** Set brightness of wheel LEDs (used for settings previews) */
    public void SetBrightness(byte brightness) {
      this.setBrightness(brightness);
    }

    /** Set animation for key */
    public LedAnimation SetAnimation(string key, string animation) {
      LedAnimation result = this.getAnimation(animation);

      switch (key) {
        case "startup": this.startupAnimation = result; break;
        case "pitLane": this.pitLaneAnimation = result; break;
        case "redLine": this.redLineAnimation = result; break;
        case "neutral": this.neutralAnimation = result; break;
        case "reverse": this.reverseAnimation = result; break;
        default: throw new Exception(string.Format("Unknown animation key {0}", key));
      }

      return result;
    }

/******************************************************************************\
 *                              UTILITY METHODS                               *
\******************************************************************************/
    /** Determine what the appropriate animation to play for a given frame of game data is */
    private LedAnimation getAnimationToPlay(StatusDataBase data, double rpmPct) {
      // If any of the game data indicates we're in the pit lane, play that animation,
      // otherwise move on to calculated redline percentage
      if ((data.IsInPit != 0 || data.IsInPitLane != 0 || data.IsInPitSince != 0)) {
        return this.pitLaneAnimation;
      }

      string gear = data.Gear.ToUpper();

      // Try to get current gear number for forward gears
      if (Int32.TryParse(gear, out int gearNo)) {
        // If at redline, play the shift point animation *unless* in top gear, Neutral, or Reverse
        if (this.revLeds.IsAtRedline(rpmPct) && gearNo < data.CarSettings_MaxGears) {
          return this.redLineAnimation;
        }
      } else if (gear == "N") { // Car is in neutral
        return this.neutralAnimation;
      } else { // Car is in reverse
        return this.reverseAnimation;
      }

      return LedAnimation.None;
    }

    /** Initialize animation parameters from game settings */
    private void initializeAnimation(ref LedAnimation animation, AnimationSettings settings) {
      animation = this.getAnimation(settings.name);
      animation.BlinkRate = settings.blinkRate;
      animation.Brightness = settings.brightness;
    }

    /** Set the current animation to the one with the matching name */
    private LedAnimation setCurrentAnimation(
      string animationName,
      int?   blinkRate         = null,
      byte?  brightness        = null
    ) {
      return this.setCurrentAnimation(this.getAnimation(animationName), blinkRate, brightness);
    }

    /** Set the current animation to the provided animation, stopping any previously playing animation (if any) */
    private LedAnimation setCurrentAnimation(
      LedAnimation desiredAnimation,
      int?         blinkRate         = null,
      byte?        brightness        = null
    ) {
      if (this.currentAnimation != desiredAnimation) {
        this.currentAnimation.stop();
        this.currentAnimation = desiredAnimation;
      }

      this.currentAnimation.BlinkRate = blinkRate ?? this.currentAnimation.BlinkRate;
      this.currentAnimation.Brightness = brightness ?? this.currentAnimation.Brightness;

      this.setBrightness(this.currentAnimation.Brightness);

      return this.currentAnimation;
    }

    /** Get the LedAnimation with a matching name */
    private LedAnimation getAnimation(string name) {
      return this.availableAnimations.FirstOrDefault(animation => animation.name == name)?.Clone() ?? LedAnimation.None;
    }

    /** Get the ShiftLightsTechnique with a matching name */
    private ShiftLightsTechnique getTechnique(string name) {
      return this.availableTechniques.FirstOrDefault(technique => technique.name == name) ?? ShiftLightsTechnique.None;
    }
  }
}
