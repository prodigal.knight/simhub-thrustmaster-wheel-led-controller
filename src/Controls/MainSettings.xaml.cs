using SimHub.ThrustmasterLEDControllerPlugin.Util;
using SimHub.ThrustmasterLEDControllerPlugin.Wheels;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace SimHub.ThrustmasterLEDControllerPlugin.Controls {
  public partial class MainSettings : UserControl, INotifyPropertyChanged {
    public ThrustmasterLEDControllerPlugin Plugin { get; }
    
    private readonly Poller updateScreenOnDeviceChange;

    // Get title for main section
    public string MainSectionTitle  => this.connectedToWheel
      ? string.Format("{0} Settings for {1}", this.Wheel.Name, this.Plugin.PluginManager.GameName)
      : "Default Wheel Settings"
      ;
    public string SaveAsDefaultText => this.connectedToWheel
      ? "Save as Default Settings"
      : "Save Default Settings"
      ;
    public bool EditingGameSettings => this.connectedToWheel;

    public Wheel Wheel => this.Plugin.Wheel ?? this.Plugin.DefaultWheel;
    
    public event PropertyChangedEventHandler PropertyChanged;

    private bool connectedToWheel = false;

    public MainSettings(ThrustmasterLEDControllerPlugin plugin) {
      this.Plugin = plugin;

      this.updateScreenOnDeviceChange = new Poller(
        50,
        () => this.Plugin.IsConnectedToWheel != this.connectedToWheel,
        _ => {
          Logger.info("Detected wheel change to {0}", this.Wheel.Name);

          // Update all properties on this control & children
          this.NotifyAllPropertiesChanged();

          this.connectedToWheel = this.Plugin.IsConnectedToWheel;
        }
      );

      this.IsVisibleChanged += (object sender, DependencyPropertyChangedEventArgs e) => {
        if (this.IsVisible) {
          // When the settings UI becomes visible, refresh all properties and start polling for device changes
          this.NotifyAllPropertiesChanged();

          this.updateScreenOnDeviceChange.Start();
        } else {
          // Stop polling for device changes
          this.updateScreenOnDeviceChange.Stop();
        }
      };

      this.InitializeComponent();

      this.DataContext = this;

      // Update all properties on this control & children
      this.NotifyAllPropertiesChanged();
    }

    /** Manually save current plugin settings */
    private void SavePluginSettings(object sender, EventArgs e) {
      this.Plugin.SaveCurrentPluginSettings();
    }

    /** Manually save current game settings */
    private void SaveCurrentSettings(object sender, EventArgs e) {
      this.Plugin.SaveCurrentGameSettings();
    }

    /** Save current game settings as defaults */
    private void SaveCurrentSettingsAsDefault(object sender, EventArgs e) {
      this.Plugin.SaveCurrentSettingsAsDefault();
    }

    private void NotifyAllPropertiesChanged() {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));

      // NOTE: Update all sub-controls as well
      this.NightDayDetection.NotifyAllPropertiesChanged();
      this.ShiftLightsControl.NotifyAllPropertiesChanged();
      this.StartupAnimationControl.NotifyAllPropertiesChanged();
      this.PitLaneAnimationControl.NotifyAllPropertiesChanged();
      this.RedLineAnimationControl.NotifyAllPropertiesChanged();
      this.NeutralAnimationControl.NotifyAllPropertiesChanged();
      this.ReverseAnimationControl.NotifyAllPropertiesChanged();
    }

    private void NotifyPropertyChanged(params string[] propertyNames) {
      foreach (string propertyName in propertyNames) {
        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }
}
