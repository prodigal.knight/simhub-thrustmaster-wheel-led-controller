﻿using Newtonsoft.Json.Linq;
using SimHub.ThrustmasterLEDControllerPlugin.Util;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  internal class PluginSettings : VersionedSettings<PluginSettings> {
    /** Migrate from an older plugin version's settings to the current plugin version's settings without losing data */
    public static PluginSettings Migrate(JObject fileSettings, PluginSettings defaultSettings = null) {
      PluginSettings settings = defaultSettings?.Clone() ?? new PluginSettings();

      if (fileSettings == null) {
        return settings;
      }

#pragma warning disable IDE0059 // Unnecessary assignment of a value
      // NOTE: May use in the future
      Version filePluginVersion     = PluginSettings.ParsePluginVersionFromFile(fileSettings);
#pragma warning restore IDE0059 // Unnecessary assignment of a value

      settings.devicePollingRate    = PluginSettings.ReadValueFromRaw(
        fileSettings,
        "devicePollingRate",
        settings.devicePollingRate
      );
      settings.ledDeactivationDelay = PluginSettings.ReadValueFromRaw(
        fileSettings,
        "ledDeactivationDelay",
        settings.ledDeactivationDelay
      );
      settings.latitude             = PluginSettings.ReadValueFromRaw(
        fileSettings,
        "latitude",
        settings.latitude
      );
      settings.longitude            = PluginSettings.ReadValueFromRaw(
        fileSettings,
        "longitude",
        settings.longitude
      );
      settings.sunAngle             = PluginSettings.ReadValueFromRaw(
        fileSettings,
        "sunAngle",
        settings.sunAngle
      );

      settings.defaultWheelSettings = WheelSettings.Migrate(
        PluginSettings.ReadValueFromRaw<JObject>(fileSettings, "defaultWheelSettings", null)
      );

      return settings;
    }

    public int    devicePollingRate    = 1000;
    public int    ledDeactivationDelay = 5000;
    public double latitude             = 0;
    public double longitude            = 0;
    public string sunAngle             = "official";

    public WheelSettings defaultWheelSettings = new WheelSettings();

    public PluginSettings() {}

    public PluginSettings(
      int           devicePollingRate,
      int           ledDeactivationDelay,
      double        latitude,
      double        longitude,
      string        sunAngle,

      WheelSettings defaultWheelSettings
    ) {
      this.devicePollingRate    = devicePollingRate;
      this.ledDeactivationDelay = ledDeactivationDelay;
      this.latitude             = latitude;
      this.longitude            = longitude;
      this.sunAngle             = sunAngle;

      this.defaultWheelSettings = defaultWheelSettings;
    }

    public override PluginSettings Clone() {
      return new PluginSettings {
        devicePollingRate    = this.devicePollingRate,
        ledDeactivationDelay = this.ledDeactivationDelay,
        latitude             = this.latitude,
        longitude            = this.longitude,
        sunAngle             = this.sunAngle,

        defaultWheelSettings = this.defaultWheelSettings.Clone()
      };
    }
  }
}
