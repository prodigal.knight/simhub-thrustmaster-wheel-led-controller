using System;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  /** Utility logger which defaults to formatted messages since that's more like what I'm used to (printf) */
  public static class Logger {
    public static void debug(string msg, params object[] args) {
      Logging.Current.DebugFormat(getMessage(msg), args);
    }

    public static void info(string msg, params object[] args) {
      Logging.Current.InfoFormat(getMessage(msg), args);
    }

    public static void warn(string msg, params object[] args) {
      Logging.Current.WarnFormat(getMessage(msg), args);
    }

    public static void error(string msg, params object[] args) {
      Logging.Current.ErrorFormat(getMessage(msg), args);
    }

    public static void error(Exception e) {
      Logging.Current.ErrorFormat(getMessage("An error occurred: {0}"), e.ToString());
    }

    private static string getMessage(string msg) {
      return string.Format("ThrustmasterLEDController: {0}", msg);
    }
  }
}
