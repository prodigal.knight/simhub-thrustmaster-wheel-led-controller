# SimHub Thrustmaster LED Controller Plugin

This is a SimHub plugin to add support for controlling the LEDs on [Thrustmaster](https://www.thrustmaster.com/) wheels.

Currently supported:

- Thrustmaster Ferrari 488 Challenge Edition

I may add support for additional wheels in the future, if someone is willing to provide USB packet captures of their own wheels (in which case they must be willing to test changes to add support for their wheel), or if I end up buying them myself.

Others are also welcome to open merge requests adding support for more wheels, including those from other manufacturers.

## Installation from package

[Release Packages](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/packages) are available as ZIP files.

Copy `SimHub.ThrustmasterLEDControllerPlugin.dll` from the package to your SimHub installation directory (e.g. `C:\Program Files (x86)\SimHub`), then (re)start SimHub. If you have not installed the plugin before, SimHub will ask for confirmation to enable the new plugin.

## Plugin Configuration

The plugin can be configured using the `Thrustmaster LED Controller` tab (whether in the left bar or under `Additional Plugins`).

Settings are saved on a game-by-game basis. Starting a new game which you have not configured before will inherit your default settings.

Animations can be individually enabled or disabled, and the animation speed of each animation is independent of others.

### Shift Light Techniques

| Name              | Description                                                           |
|------------------:|:----------------------------------------------------------------------|
| leftToRight       | RPM shiftlights turn on from left to right as RPM increases           |
| leftToRightThirds | RPM shiftlights turn on in thirds from left to right as RPM incleases |
| edgeToCenter      | RPM shiftlights turn on from the edges towards the center             |
| centerToEdge      | RPM shiftlights turn on from the center towards the edges             |

I may add support for KERS in shift light techniques at some point in the future but, I currently don't play any games which use KERS, so it's not a priority for me. Merge requests adding KERS support are welcome.

### RPM Scaling Techniques

| Name              | Description                                                                      | Formula                                |
|------------------:|:---------------------------------------------------------------------------------|:---------------------------------------|
| linear            | RPM percentage is used as-is without modification                                | `x => x`                               |
| logarithmic       | RPM percentage is transformed to the natural logarithm and scaled to roughly fit | `x => (ln(x) + 2) / 2`                 |
| logarithmish      | RPM percentage is transformed to a nearly logarithmic curve and scaled to fit    | `x => (((2x - 1) / (2x + 1)) + 2) / 2` |
| exponential       | RPM percentage is squared to form a curve                                        | `x => x^2`                             |

### Animations

Currently, animations may be set for the following events.

| Name    | Description                                                      |
|--------:|:-----------------------------------------------------------------|
| startup | Played when SimHub first starts or when starting a game          |
| pitLane | Played when you are in the pit lane                              |
| redLine | Played when RPM is at or above the configured redline percentage |
| neutral | Played when the vehicle is in Neutral                            |
| reverse | Played when the vehicle is in Reverse                            |

The following is a list of animations which can be used for each event.

| Name               | Description                                     |
|-------------------:|:------------------------------------------------|
| none               | Do not play an animation                        |
| alternating        | Alternates between every other LED on the wheel |
| blinking           | Blinks all wheel LEDs at once                   |
| blinkingCenter     | Blinks the center LEDs only                     |
| blinkingEdges      | Blinks the outer LEDs only                      |
| crossing           | Blinks from edges towards & through center      |
| exploding          | Blinks from center to edges and back again      |
| leftToRight        | Blinks each LED going from left to right        |
| rightToLeft        | Blinks each LED going from right to left        |
| blink `N`th led    | Blinks the `N`th LED from the left              |

## Providing Packet Captures to Add Support for More Wheels

> If you do this, you *must* be willing to test occasional beta builds of the plugin

If you have a wheel which this plugin does not support and you would like me to add support, you can provide me with USB packet capture files so that I may look at the data being sent across the wire and attempt to reverse engineer a solution for LED control.

In order to provide USB packet capture files, you should install [Wireshark](https://www.wireshark.org/), making sure to install USBPcap. If you already have Wireshark installed, make sure you have USBPcap installed with it, and if you don't, re-run the installer in order to add it to your system.

Once you have Wireshark/USBPcap installed, you will need to determine which USB interface your wheel is connected to. You can determine this by capturing on all USB interfaces and looking at the `DEVICE DESCRIPTOR` field in `DESCRIPTOR Response DEVICE` packets at the beginning of the capture until you find one with `idVendor: Thrustmaster, Inc. (0x044f)` (or whatever is appropriate for your wheel's manufacturer).

At this point, what I request is that you save a separate USB packet capture file for each function that can be performed on the wheel's LEDs, e.g.: if your wheel has colorizable LEDs with adjustable brightness:

- 1 file where you edit the color of several/all LEDs
- 1 file where you edit the brightness of several/all LEDs
- 1 file where various LEDs are illuminated (e.g.: playing a test pattern in the vendor's device control software)

Once you have these, you can open an issue, stating which wheel you have, and attach your packet capture files to it.

## Building from source

Open `SimHub.ThrustmasterLEDControllerPlugin.sln` with Visual Studio 2019 or later.
