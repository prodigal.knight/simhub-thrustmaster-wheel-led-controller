﻿using Newtonsoft.Json.Linq;

namespace SimHub.ThrustmasterLEDControllerPlugin.Settings {
  public abstract class SettingsCommon<T> where T : SettingsCommon<T> {
    /**
     * Get value from file, or default if no value exists
     *
     * @param  {JObject} file         Parsed JSON file
     * @param  {string}  key          File setting key
     * @param  {K}       defaultValue Default to use if no value exists for key in file
     *
     * @return K
     */
    public static K ReadValueFromRaw<K>(JObject file, string key, K defaultValue) {
      if (key == null) {
        return defaultValue;
      }

      return (K) (file[key]?.ToObject(typeof(K)) ?? defaultValue);
    }

    public abstract T Clone();
  }
}
