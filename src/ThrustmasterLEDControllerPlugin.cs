﻿using GameReaderCommon;
using HidLibrary;
using Newtonsoft.Json.Linq;
using SimHub.Plugins;
using SimHub.ThrustmasterLEDControllerPlugin.Controls;
using SimHub.ThrustmasterLEDControllerPlugin.Settings;
using SimHub.ThrustmasterLEDControllerPlugin.Util;
using SimHub.ThrustmasterLEDControllerPlugin.Wheels;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SimHub.ThrustmasterLEDControllerPlugin {
  [PluginDescription("LED controller plugin for Thrustmaster wheels")]
  [PluginAuthor     ("Prodigal.Knight")                              ]
  [PluginName       ("Thrustmaster LED Controller")                  ]
  public class ThrustmasterLEDControllerPlugin : IPlugin, IDataPlugin, IWPFSettings {
    public static string PluginVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

    public readonly string PluginSettingsName = "main";

    public  PluginManager PluginManager { get; set; }

    /** Used by the settings page to prevent DataUpdate from acting on LEDs */
    public  readonly SemaphoreLock LedLock = new SemaphoreLock();

    public  bool  IsConnectedToWheel => this.wheel != null;
    public  Wheel Wheel              => this.wheel;
    public  Wheel DefaultWheel       => this.defaultWheel;

    public  int   DevicePollingRate {
      get => this.devicePoller.Rate;
      set => this.devicePoller.Rate = value;
    }

    public  int   LedDeactivationDelay {
      get => this.ledDeactivationTimeout.Delay;
      set => this.ledDeactivationTimeout.Delay = value;
    }
    
    private readonly int   ThrustmasterVendorId   = 0x044F;
    private readonly int[] ThrustmasterProductIds = {
      0xB689, // Thrustmaster TS-PC Racer
      // Additional Thrustmaster product IDs I found in the decompiled source code for the built-in Thrustmaster plugin.
      // I don't know which bases these correspond to.
      0xB68A,
      0xB66F,
      0xB669,
      0xB67F,
      0xB66E,
      0xB65E,
      0xB662,
      0xB692,
      0xB684,
      0xB696
    };

    private readonly Poller<Wheel>    devicePoller;    
    private readonly NightDayDetector nightDayDetector;

    public  NightDayDetector NightDayDetector => this.nightDayDetector;

    private Wheel wheel;
    private readonly DefaultWheel defaultWheel = new DefaultWheel();

    private readonly Timeout ledDeactivationTimeout;

    public ThrustmasterLEDControllerPlugin() {
      Logger.info("Reading saved plugin settings");
      PluginSettings settings = PluginSettings.Migrate(
        this.ReadCommonSettings<JObject>(this.PluginSettingsName, () => null)
      );

      this.defaultWheel.CurrentSettings = settings.defaultWheelSettings;

      this.ledDeactivationTimeout = new Timeout(this.OnGamePausedTimeoutElapsed, settings.ledDeactivationDelay);

      Logger.info("Setting up device poller");
      this.devicePoller = new Poller<Wheel>(
        settings.devicePollingRate,
        this.pollForCompatibleDevices,
        this.onCompatibleDeviceFound
      );

      Logger.info("Setting up night/day detector");
      this.nightDayDetector = new NightDayDetector(
        settings.latitude,
        settings.longitude,
        settings.sunAngle
      );
    }

    /** Process a game data update */
    public void DataUpdate(PluginManager pluginManager, ref GameData data) {
      if (this.LedLock.IsLocked || this.wheel == null) {
        return; // Don't process game data while editing a setting or if there's no wheel plugged in
      }

      if (!data.GameRunning || data.GamePaused) { // If the game is paused, don't process game data normally
        // If the game was just paused, note the time, pause current animation
        if (!this.ledDeactivationTimeout.Running) {
          this.wheel.PauseAnimation();

          this.ledDeactivationTimeout.Start();
        }
      } else {
        this.ledDeactivationTimeout.Stop();

        _ = this.wheel.ProcessGameData(data.NewData, this.nightDayDetector);
      }
    }

    /** Initialize the plugin */
    public void Init(PluginManager pluginManager) {
      this.PluginManager = pluginManager;
      this.devicePoller.Start(stopOnResultFound: true);
    }

    /** Close/cleanup the plugin/wheel */
    public void End(PluginManager pluginManager) {
      this.devicePoller.Stop();

      Logger.info("Disconnecting from wheel");
      _ = this.wheel?.Deactivate();
      this.wheel = null;

      this.LedLock.Reset();
    }

    /** Get plugin settings controller */
    Control IWPFSettings.GetWPFSettingsControl(PluginManager pluginManager) {
      return new MainSettings(this);
    }

    /** Save current plugin settings to a file */
    public void SaveCurrentPluginSettings() {
      Logger.info("Saving plugin settings");
      this.SaveCommonSettings(
        this.PluginSettingsName,
        new PluginSettings(
          this.DevicePollingRate,
          this.LedDeactivationDelay,
          this.nightDayDetector.Latitude,
          this.nightDayDetector.Longitude,
          this.nightDayDetector.SunAngle,

          this.defaultWheel.CurrentSettings
        )
      );
    }

    /** Save current game settings to file */
    public void SaveCurrentGameSettings() {
      if (this.wheel != null) {
        Logger.info("Saving wheel settings for game {0}", this.PluginManager.GameName);
        this.SaveCommonSettings(this.PluginManager.GameName, this.wheel.CurrentSettings);
      } else {
        Logger.error("No wheel connected");
      }
    }

    /** Update and save default wheel settings */
    public void SaveCurrentSettingsAsDefault() {
      Logger.info("Updating default wheel settings");
      this.defaultWheel.CurrentSettings = this.wheel?.CurrentSettings?.Clone() ?? this.defaultWheel.CurrentSettings;

      this.SaveCurrentPluginSettings();
    }

    /**
     * Called by device poller to search for connected Thrustmaster wheels. Returns a wheel object if one was found,
     * otherwise null.
     */
    private Wheel pollForCompatibleDevices() {
      HidDevice device = HidDevices.Enumerate(this.ThrustmasterVendorId, this.ThrustmasterProductIds).FirstOrDefault();

      if (device != null) {
        Logger.info("Found a Thrustmaster wheel");
        // TODO: Determine which wheel it is and return only if it's got
        // controllable LEDs

        // Not sure if there's actually an official way to determine which wheel
        // is plugged in beyond some PCAP captures I've done demonstrating a
        // distinct response for one magic instruction to the control endpoint
        // on the servo motor with different wheels plugged in. If there's an
        // official way to determine this, I have yet to find documentation for
        // it.

        // Also not sure if it matters as the wheel *should* ignore commands it
        // doesn't understand... maybe it sends back a packet indicating failure?
        // In which case I could send a brightness or LED activation command here
        // and see if it responds with success...

        device.MonitorDeviceEvents = true;
        device.Removed += _onDeviceRemoved;

        return new Thrustmaster488ChallengeEdition(device);
      }

      return null;
    }

    /** Called by the device poller when a compatible wheel was found, with a reference to the wheel */
    private async Task<bool> onCompatibleDeviceFound(Wheel wheel) {
      try {
        Logger.info("Reading saved settings for game {0}", this.PluginManager.GameName);
        WheelSettings settings = WheelSettings.Migrate(
          this.ReadCommonSettings<JObject>(this.PluginManager.GameName, () => null),
          this.defaultWheel.CurrentSettings
        );

        Logger.info("Initializing wheel with saved settings");
        await wheel.Init(settings);

        Logger.info("Wheel finished initializing");
        this.wheel = wheel;

        return true;
      } catch (Exception e) {
        Logger.error(e);
        
        return false;
      }
    }

    /** Called by HidDevice Removed listener when a connected device has been removed by the system */
    private void _onDeviceRemoved() {
      Logger.info("Wheel disconnected");
      // NOTE: Wheel should never be null here but just in case, use null-safe access
      _ = this.wheel?.Deactivate();
      this.wheel = null;

      this.LedLock.Reset();

      this.devicePoller.Start(stopOnResultFound: true);
    }
    
    private void OnGamePausedTimeoutElapsed() {
      this.wheel.ClearAnimation();
      _ = this.wheel.TurnOffLEDs();
    }
  }
}
