# Change Log

All notable changes to this project shall be documented in this file.

## [1.2.2](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.2.1...1.2.2?from_project_id=43240488&straight=true) (2023-12-31)

### Bugfixes

- Fixes enabled toggle for animations ([063e5e34](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/063e5e34c890a754da17db711036cdb777f4fa4e))
- Replaces animation tooltips which were missed in last release ([063e5e34](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/063e5e34c890a754da17db711036cdb777f4fa4e))

### Miscellaneous

- Extracts additional utility classes Timeout and Interval to avoid Poller overuse in code base ([063e5e34](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/063e5e34c890a754da17db711036cdb777f4fa4e))
- Simplifies codebase by making unnecessarily private members public and removing getter/setter implementations for them ([063e5e34](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/063e5e34c890a754da17db711036cdb777f4fa4e))

## [1.2.1](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.2.0...1.2.1?from_project_id=43240488&straight=true) (2023-09-08)

### Bugfixes

- Loads redline animation correctly post-upgrade ([e4827449](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/e4827449fb2225831b28bf5f1ad1bd43628206d1))

## [1.2.0](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.1.4...1.2.0?from_project_id=43240488&straight=true) (2023-09-08)

### Features

- Animations can now have a separate brightness setting from Rev LEDs ([bb86142c](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/bb86142cd3f6f6d1b3e45992a867bc479d448433))
- Single LED blink animations are now available ([8b814c70](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/8b814c70cfd06014d779b1b25d24997b19e62259))
- Adds detection for when the car is in neutral or reverse and animations for those states ([2e202424](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/2e202424902aeab4b812aba47498062193226038))
- Adds the ability to save current settings as defaults which are inherited automatically by new games ([4c9cb8a4](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/4c9cb8a43feb8d46a7d98c73e0fe04db373e8cb8))

### Miscellaneous

- Settings are no longer automatically saved when switching games in SimHub ([4c9cb8a4](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/4c9cb8a43feb8d46a7d98c73e0fe04db373e8cb8))
- Settings screen overhaul to adjust to new code
- Project files were reorganized in order to better facilitate future development
- Some manually tracked state changes were abstracted into objects which track smaller scopes and are easier to test

## [1.1.4](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.1.3...1.1.4?from_project_id=43240488&straight=true) (2023-06-07)

### Bugfixes

- RPM Scaling Techniques now properly account for non-100% value for max RPM in settings screen ([a71bf5ff](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a71bf5ff2f04aae2640a10d89a3a59f8608cfdeb))
- Fixes issue which caused SimHub to forget saved animation/shift light technique settings when disconnecting/reconnecting a wheel ([01da6736](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/01da67361a5e8343347b10408a86a7859885787d))

### Miscellaneous

- Improves handling of settings screen update/refresh when a wheel is connected or disconnected ([da1befae](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/da1befae511f8d052a8fecded0ead9cc1c2d0a5f))
- Refactors Wheel interface as an abstract class in order to facilitate adding handling for additional wheels in the future ([ecc14416](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/ecc14416a133f58d80d191b62075843b52e8e2c0))

## [1.1.3](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.1.2...1.1.3?from_project_id=43240488&straight=true) (2023-03-20)

### Features

- Adds RPM scaling methods in order to alter how fast shift lights "update" in relationship to RPM percentage ([0e26d576](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/0e26d5766203bed493dc09ca013adacc56f38407))

### Miscellaneous

- Settings screen now refreshes sunrise/sunset periodically in the background ([42efe5d6](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/42efe5d60f0d1e42a737ae074dbcdb1ccb2cd7db))
- Fixes link to timeanddate.com by sending 3 decimal places of latitude & longitude ([a85326c5](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a85326c5906d0bb60ea71309fea55ad7d6b71fa1))
- Puts seconds in sunrise/sunset times ([a85326c5](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a85326c5906d0bb60ea71309fea55ad7d6b71fa1))

## [1.1.2](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.1.1...1.1.2?from_project_id=43240488&straight=true) (2023-03-17)

### Features

- Sunrise/sunset calculation for automatic night/day mode switching on shift lights ([61883d64](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/61883d64472aec23eef3af0fd546480e57120026), [cce8484c](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/cce8484c281bc4c763252575f9800e1fcaca95ad))

### Removed

- Button on settings page to manually force check for connected wheel (no longer necessary with device poller) ([3563a0f2](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/3563a0f2cf86d57708d28ecb71d56b875e9f4b1d))

### Miscellaneous

- Tooltips added to most labels on settings page ([a8518307](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a85183072c323773afb725cd004cc87b16422ad5))

## [1.1.1](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.1.0...1.1.1?from_project_id=43240488&straight=true) (2023-03-10)

### Bugfixes

- Current animation is paused when the game is paused ([88683d7c](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/88683d7c94ef6dc9ebffa14e5ccbd53d7e9366ae))

### Features

- Adds "Night Mode" option to Shift Lights techniques (modelled after FanaLEDs "nightmode" revled styles) ([f3b1bae4](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/f3b1bae48068adfcc0d0c8df8cebb4b8198f6a77))

### Miscellaneous

- Settings page waits for wheel to finish initializing before becoming available ([cadf023c](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/cadf023c021170a735a46ac9af79a4862b1f10df), [4ee0f30b](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/4ee0f30b8fe3068664b2aed447de9528c331ca8c), [a239d123](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a239d123460a45bab917288cb01b8e55ce57107e))
- Wheel initialized using settings object instead of passing individual values ([3a3465f5](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/3a3465f529a29b924a289603a034371513deba0c))

## [1.1.0](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.2.2...1.1.0?from_project_id=43240488&straight=true) (2023-03-06)

### Bugfixes

- Blinking animations start with blank frame ([cbd15237](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/cbd15237ab406bd492c6c263fd556ada9dbb9912))

### Features

- Per-animation blink rates ([cfe86645](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/cfe866459b6ba05753c19d6a25379a6e87d1e906))
- Maximum RPM Percentage can be set higher than 100% ([cfe86645](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/cfe866459b6ba05753c19d6a25379a6e87d1e906))
- Plugin-wide settings for device polling rate and LED deactivation delay when game is paused ([d9a72dcc](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/d9a72dcc8d92e7e960fec1ecc5d85b3d537a693d))

## [1.0.2](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.0.1...1.0.2?from_project_id=43240488&straight=true) (2023-02-15)

### Bugfixes

- Device handle is disposed when wheel is disconnected. ([040ba701](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/040ba701c031f36ad6cdbe2de552c0244376b689))

### Features

- Polls for devices when no wheel is connected ([040ba701](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/040ba701c031f36ad6cdbe2de552c0244376b689))

## [1.0.1](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/compare/1.0.0...1.0.1?from_project_id=43240488&straight=true) (2023-02-08)

### Bugfixes

- Animations can no longer be stopped & restarted during the space of a single frame while they are still playing, causing a perceived increase in animation speed. ([a7de508f](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/a7de508ffbd659e5e176025cff1981481f474e79))

### New Features

- When changing shift light techniques, a preview is played on the wheel. ([5e0478b2](https://gitlab.com/prodigal.knight/simhub-thrustmaster-wheel-led-controller/-/commit/5e0478b235967f61bbd7239446b28ed2e2455e9d))
