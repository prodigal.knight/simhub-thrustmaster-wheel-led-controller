﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SimHub.ThrustmasterLEDControllerPlugin")]
[assembly: AssemblyDescription("Thrustmaster LED Controller plugin for SimHub")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SimHub.ThrustmasterLEDControllerPlugin")]
[assembly: AssemblyCopyright("Copyright © 2023-2024")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("72dc5684-9c2c-486d-b4ab-48b4c21d1e45")]
[assembly: AssemblyVersion("1.2.2")]
[assembly: AssemblyFileVersion("1.2.2")]

#if(DEBUG)
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
