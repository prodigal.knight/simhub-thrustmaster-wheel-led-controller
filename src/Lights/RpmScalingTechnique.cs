﻿using System;
using System.Linq;

namespace SimHub.ThrustmasterLEDControllerPlugin.Lights {
  public class RpmScalingTechnique {
    public static RpmScalingTechnique Linear       = new RpmScalingTechnique(
      "linear",
      v => v
    );
    public static RpmScalingTechnique Logarithmic  = new RpmScalingTechnique(
      "logarithmic",
      v => (Math.Max(Math.Log(v), double.MinValue) + 2) / 2
    );
    // NOTE: Approximate logarithmic scaling. Formula taken from https://stats.stackexchange.com/a/580870
    public static RpmScalingTechnique Logarithmish = new RpmScalingTechnique(
      "logarithmish",
      v => (2 * (v - 1) / (v + 1) + 2) / 2
    );
    public static RpmScalingTechnique Exponential  = new RpmScalingTechnique(
      "exponential",
      v => Math.Pow(v, 2)
    );

    public static readonly RpmScalingTechnique[] techniques = {
      RpmScalingTechnique.Linear,
      RpmScalingTechnique.Logarithmic,
      RpmScalingTechnique.Logarithmish,
      RpmScalingTechnique.Exponential,
    };

    public static RpmScalingTechnique getTechnique(string techniqueName) {
      return techniques.FirstOrDefault(technique => technique.name == techniqueName) ?? RpmScalingTechnique.Linear;
    }

    public readonly string               name;
    public readonly Func<double, double> scale;

    public RpmScalingTechnique(string name, Func<double, double> scale) {
      this.name  = name;
      this.scale = scale;
    }
  }
}
