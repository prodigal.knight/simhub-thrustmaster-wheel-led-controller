﻿using System;
using System.Linq;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  public class SunAngle {
    public readonly string name;
    public readonly double angle;

    /**
     * @constructor
     *
     * @param {string} name
     * @param {double} angle
     */
    public SunAngle(string name, double angle) {
      this.name  = name;
      this.angle = angle;
    }
  }

  public class NightDayDetector {
    private static readonly SunAngle[] sunAngles = {
      new SunAngle("official",     -0.8333),
      new SunAngle("civil",        -6),
      new SunAngle("nautical",     -12),
      new SunAngle("astronomical", -18),
      new SunAngle("bottomdisc",   -0.2998),
    };

    public  static readonly string[] SunAngles         = NightDayDetector.sunAngles
      .Select(sunAngle => sunAngle.name)
      .ToArray();

    private static readonly double   EarthPerihelion   = Conversions.ToRadians(102.9382);
    private static readonly double   EarthObliquity    = Conversions.ToRadians(23.4392911);

    private static readonly double   sinEarthObliquity = Math.Sin(NightDayDetector.EarthObliquity);
    private static readonly double   cosEarthObliquity = Math.Cos(NightDayDetector.EarthObliquity);

    public string SunAngle {
      get => this.angle.name;
      set {
        this.angle = this.getSunAngleByName(value);
        this.determineSunriseAndSunsetForToday();
      }
    }

    public double Latitude {
      get => this.latitude;
      set {
        this.latitude = value;
        this.phi = Conversions.ToRadians(this.latitude);
        this.determineSunriseAndSunsetForToday();
      }
    }

    public double Longitude {
      get => this.longitude;
      set {
        this.longitude = value;
        this.lw = Conversions.ToRadians(this.longitude);
        this.determineSunriseAndSunsetForToday();
      }
    }

    public  string Sunrise => this.sunrise.ToLongTimeString();
    public  string Sunset  => this.sunset.ToLongTimeString();

    private double latitude;
    private double phi;

    private double longitude;
    private double lw;

    private SunAngle angle;

    private DateTime today   = DateTime.Now.Date;
    private DateTime sunrise;
    private DateTime sunset;

    /**
     * @constructor
     *
     * @param {double} latitude  User latitude
     * @param {double} longitude User longitude
     * @param {string} angleName Sun angle to use for calculations
     */
    public NightDayDetector(double latitude, double longitude, string angleName = "official") {
      this.latitude  = latitude;
      this.longitude = longitude;

      this.phi       = Conversions.ToRadians(this.latitude);
      this.lw        = Conversions.ToRadians(-this.longitude);

      this.angle     = this.getSunAngleByName(angleName);

      this.determineSunriseAndSunsetForToday();
    }

    /** Determine whether it is currently night or day */
    public bool IsNight() {
      DateTime now = DateTime.Now;

      this.UpdateSunriseSunset(now);

      return now < this.sunrise || now > this.sunset;
    }

    /**
     * Update today's sunrise/sunset time if the day has changed from the last time this was called
     *
     * @param  {DateTime} now
     *
     * @return bool
     */
    public bool UpdateSunriseSunset(DateTime now) {
      if (now.Date != this.today) {
        this.today = now.Date;
        this.determineSunriseAndSunsetForToday();

        return true;
      }

      return false;
    }

    /** Determine sunrise/sunset times for today */
    private void determineSunriseAndSunsetForToday() {
      // NOTE: Adapted from https://github.com/Tronald/CoordinateSharp/blob/1ffb4c01aab1e368d09470b09bb9ce174b80e2ad/CoordinateSharp/Celestial/Solar/SunCalculations.cs
      (this.sunrise, this.sunset) = this.getSunriseAndSunsetForDate(
        this.lw,
        this.phi,
        this.angle.angle,
        new DateTime(
          this.today.Year,
          this.today.Month,
          this.today.Day,
          0,
          0,
          0,
          DateTimeKind.Utc
        ),
        DateTimeOffset.Now.Offset.TotalHours
      );
    }

    /**
     * Determine sunrise and sunset for a given date
     *
     * @param  {double}   lw
     * @param  {double}   phi
     * @param  {double}   h      Sun angle relative to ideal horizon
     * @param  {DateTime} date   Today
     * @param  {double}   offset Local offset from UTC
     *
     * @return Tuple<DateTime, DateTime> Sunrise/sunset times for the given date
     */
    private Tuple<DateTime, DateTime> getSunriseAndSunsetForDate(
      double   lw,
      double   phi,
      double   h,
      DateTime date,
      double   offset
    ) {
      double julianOffset = offset * 0.04166667;

      double d   = Conversions.ToJulian(date) - Conversions.J2000 + 0.5;
      double n   = this.julianCycle(d, lw);

      double ds  = this.approximateTransit(0, lw, n);
      double M   = this.solarMeanAnomaly(ds);
      double L   = this.eclipticLongitude(M);
      double dec = this.declination(L, 0);

      double julianNoon = this.solarTransitJulian(ds, M, L);
      double julianSet  = this.calculateSunsetTime(Conversions.ToRadians(h), lw, phi, dec, n, M, L);
      double julianRise = julianNoon - (julianSet - julianNoon);

      DateTime sunrise  = Conversions.ToGregorian(julianRise + julianOffset) ?? date.AddDays(1).AddTicks(-1);
      DateTime sunset   = Conversions.ToGregorian(julianSet + julianOffset)  ?? date.Date;

      return Tuple.Create(sunrise, sunset);
    }

    private double julianCycle(double d, double lw) {
      return Math.Round(d - 0.0009 - (lw / Conversions.twoRadians));
    }

    private double approximateTransit(double Ht, double lw, double n) {
      return 0.0009 + ((Ht + lw) / Conversions.twoRadians) + n;
    }

    private double solarMeanAnomaly(double ds) {
      return Conversions.ToRadians(357.5291 + (0.98560028 * ds));
    }

    private double eclipticLongitude(double M) {
      double c = Conversions.ToRadians(
        (1.9148 * Math.Sin(M)) +
        (0.0200 * Math.Sin(2 * M)) +
        (0.0003 * Math.Sin(3 * M))
      );

      return M + c + NightDayDetector.EarthPerihelion + Math.PI;
    }

    private double declination(double L, double b) {
      return Math.Asin(
        (Math.Sin(b) * NightDayDetector.cosEarthObliquity) +
        (Math.Cos(b) * NightDayDetector.sinEarthObliquity * Math.Sin(L))
      );
    }

    private double solarTransitJulian(double ds, double M, double L) {
      return Conversions.J2000 + ds + (0.0053 * Math.Sin(M)) - (0.0069 * Math.Sin(2 * L));
    }

    private double calculateSunsetTime(double h, double lw, double phi, double dec, double n, double M, double L) {
      double approxTime = this.hourAngle(h, phi, dec);
      double a = this.approximateTransit(approxTime, lw, n);

      return this.solarTransitJulian(a, M, L);
    }

    private double hourAngle(double h, double phi, double dec) {
      return Math.Acos(
        (Math.Sin(h) - (Math.Sin(phi) * Math.Sin(dec))) /
        (Math.Cos(phi) * Math.Cos(dec))
      );
    }

    private SunAngle getSunAngleByName(string name) {
      return NightDayDetector.sunAngles.FirstOrDefault(angle => angle.name == name) ?? NightDayDetector.sunAngles[0];
    }
  }
}
