﻿using System;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  public static class Conversions {
    public  static readonly double J2000            = 2451545;

    public  static readonly double twoRadians       = 2 * Math.PI;

    private static readonly DateTime GregorianStart = new DateTime(1582, 10, 15);

    private static readonly double deg              = 180 / Math.PI;
    private static readonly double rad              = Math.PI / 180;

    /** Convert from radians to degrees */
    public static double ToDegrees(double radians) {
      return deg * radians;
    }

    /** Convert from degrees to radians */
    public static double ToRadians(double degrees) {
      return rad * degrees;
    }

    /**
     * Convert the given Gregorian date to a Julian date
     *
     * @param  {DateTime} date Gregorian date to convert
     *
     * @return double
     */
    // NOTE: this and ToGregorian adapted from https://github.com/Tronald/CoordinateSharp/blob/1ffb4c01aab1e368d09470b09bb9ce174b80e2ad/CoordinateSharp/Julian/Julian.cs
    public static double ToJulian(DateTime date) {
      double y = date.Year;
      double m = date.Month;
      double d = date.Day + (date.TimeOfDay.TotalHours / 24);

      double A = (int) (y / 100);
      double B = 0;

      if (m <= 2) {
        m += 12;
        y -= 1;
      }

      if (date >= Conversions.GregorianStart) {
        B = 2 - A + (int) (A / 4);
      }

      return (int) (365.25 * (y + 4716)) + (int) (30.6001 * (m + 1)) + d + B - 1524.5;
    }

    /**
     * Convert the given Julian date to a Gregorian date
     *
     * @param  {double} julian Julian date to convert
     *
     * @return DateTime if valid Gregorian date
     */
    public static DateTime? ToGregorian(double julian) {
      if (Double.IsNaN(julian)) {
        return null;
      }

      julian   += 0.5;
      double Z  = Math.Floor(julian);
      double F  = julian - Z;
      double A  = Z;

      if (Z >= 2299161) {
        double a = (int) ((Z - 1867216.25) / 36524.25);
        A = Z + 1 + a - (int) (a / 4);
      }

      double B  = A + 1524;
      double C  = (int) ((B - 122.1) / 365.25);
      double D  = (int) (365.25 * C);
      double E  = (int) ((B - D) / 30.6001);

      double day = B - D - (int) (30.6001 * E) + F;

      double month = E - 1;
      if (E > 13) {
        month -= 12;
      }

      double year = C - 4715;
      if (month > 2) {
        year--;
      }

      double hours   = (    day - Math.Floor(day)    ) * 24;
      double minutes = (  hours - Math.Floor(hours)  ) * 60;
      double seconds = (minutes - Math.Floor(minutes)) * 60;

      return new DateTime(
        (int) year,
        (int) month,
        (int) day,
        (int) hours,
        (int) minutes,
        (int) seconds
      );
    }
  
    /**
     * Get an ordinal string for the given number
     *
     * @param  {int} number The value to get an ordinal string for
     *
     * @return string
     */
    public static string ToOrdinal(int number) {
      if (number <= 0) {
        return number.ToString();
      }

      switch (number % 100) {
        case 11: /* falls through */
        case 12: /* falls through */
        case 13: return number + "th";
      }

      switch (number % 10) {
        case 1: return number + "st";
        case 2: return number + "nd";
        case 3: return number + "rd";
        default: return number + "th";
      }
    }
  }
}
