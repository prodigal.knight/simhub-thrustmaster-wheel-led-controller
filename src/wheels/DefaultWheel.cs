﻿using SimHub.ThrustmasterLEDControllerPlugin.Lights;
using System.Linq;

namespace SimHub.ThrustmasterLEDControllerPlugin.Wheels {
  public class DefaultWheel : Wheel {
    // Allow saving settings for any arbitrary number of rev LEDs on a wheel
    public const    byte   numRevLEDs = 255;

    public override string Name => "Default";
    public override string SettingsName => "default";

    protected override LedAnimation[] availableAnimations => new LedAnimation[]{
      LedAnimation.None,
      LedAnimation.crossingLeds(numRevLEDs),
      LedAnimation.alternatingLeds(numRevLEDs),
      LedAnimation.leftToRight(numRevLEDs),
      LedAnimation.rightToLeft(numRevLEDs),
      LedAnimation.blinkingLeds(numRevLEDs),
      LedAnimation.blinkingCenterLeds(numRevLEDs),
      LedAnimation.blinkingEndLeds(numRevLEDs),
      LedAnimation.explodingLeds(numRevLEDs),
    }
      .Concat(Enumerable.Range(0, numRevLEDs).Select(idx => LedAnimation.blinkSingleLed(idx)))
      .ToArray();
    
    protected override ShiftLightsTechnique[] availableTechniques => new ShiftLightsTechnique[]{
      ShiftLightsTechnique.None,
      ShiftLightsTechnique.ltr(numRevLEDs),
      ShiftLightsTechnique.etc(numRevLEDs),
      ShiftLightsTechnique.cte(numRevLEDs),
      ShiftLightsTechnique.ltrThirds(numRevLEDs),
    };
    
    public DefaultWheel() : base(null) {}

    protected override void setLedState(ushort _) {}
    protected override void setBrightness(int _) {}
  }
}
