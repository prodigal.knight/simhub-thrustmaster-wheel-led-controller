﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Util {
  public class Timeout {
    public  bool     Running => this.cancellationTokenSource != null;
    public  int      Delay { get; set; }

    private readonly Func<Task>              callback;

    private          CancellationTokenSource cancellationTokenSource;

    public Timeout(Action callback, int delay) : this(() => Task.Run(callback), delay) {}

    public Timeout(Func<Task> callback, int delay) {
      this.callback = callback;
      this.Delay    = delay;
    }

    public void Start() {
      this.cancellationTokenSource = new CancellationTokenSource();

      Task.Run(async () => {
        try {
          await Task.Delay(this.Delay, this.cancellationTokenSource.Token);

          _ = this.callback().ContinueWith(e => { Logger.error(e.Exception); }, TaskContinuationOptions.OnlyOnFaulted);
        } catch (TaskCanceledException) {
          // Timeout was cancelled, don't log an exception
        } catch (Exception e) {
          Logger.error(e);
        } finally {
          this.cancellationTokenSource = null;
        }
      });
    }

    public void Stop() {
      this.cancellationTokenSource?.Cancel();
    }
  }
}
