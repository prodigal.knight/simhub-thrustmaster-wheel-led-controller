﻿using SimHub.ThrustmasterLEDControllerPlugin.Settings;
using SimHub.ThrustmasterLEDControllerPlugin.Util;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SimHub.ThrustmasterLEDControllerPlugin.Lights {
  public enum NightMode { Off, Auto, On }

  public class RevLeds {
    public byte Brightness {
      get => this.brightness;
      set => this.brightness = value;
    }

    public double MinRpmPct {
      get => this.minRpmPct;
      set {
        this.minRpmPct       = value;
        this.scaledMinRpmPct = this.scalingTechnique.scale(this.minRpmPct);
      }
    }
    public double MaxRpmPct {
      get => this.maxRpmPct;
      set {
        this.maxRpmPct       = value;
        this.scaledMaxRpmPct = this.scalingTechnique.scale(this.maxRpmPct);
      }
    }

    public string ScalingMethod {
      get => this.scalingTechnique.name;
      set {
        this.scalingTechnique = RpmScalingTechnique.getTechnique(value);
        this.scaledMinRpmPct  = this.scalingTechnique.scale(this.minRpmPct);
        this.scaledMaxRpmPct  = this.scalingTechnique.scale(this.maxRpmPct);
      }
    }

    public NightMode NightMode {
      get => this.nightMode;
      set => this.nightMode = value;
    }

    private readonly Action<ushort> setLedState;

    private RpmScalingTechnique scalingTechnique = RpmScalingTechnique.Linear;
    private NightMode           nightMode        = NightMode.Off;
    private byte                brightness       = 50;
    private double              minRpmPct        = 0.5;
    private double              maxRpmPct        = 0.5;
    private double              scaledMinRpmPct  = 0.5;
    private double              scaledMaxRpmPct  = 0.5;

    /**
     * @constructor
     *
     * @param {Action<ushort>} setLedState
     */
    public RevLeds(Action<ushort> setLedState) {
      this.setLedState = setLedState;
    }

    /**
     * Initialize from settings
     *
     * @param  {RevLedsSettings} settings
     *
     * @return void
     */
    public void Init(RevLedsSettings settings) {
      this.brightness       = settings.brightness;
      this.nightMode        = settings.nightMode;
      this.minRpmPct        = settings.minRpmPct;
      this.maxRpmPct        = settings.maxRpmPct;
      this.scalingTechnique = RpmScalingTechnique.getTechnique(settings.rpmScaling);
      this.scaledMinRpmPct  = this.scalingTechnique.scale(minRpmPct);
      this.scaledMaxRpmPct  = this.scalingTechnique.scale(maxRpmPct);
    }

    /**
     * Check to see if the given value is above redline
     *
     * @param  {double} value Value to check
     *
     * @return bool
     */
    public bool IsAtRedline(double value) {
      return value >= this.maxRpmPct;
    }

    /**
     * Display shift lights for the current RPM percentage
     *
     * @param  {double}               rpmPct           Current RPM percentage
     * @param  {NightDayDetector}     nightDayDetector Night/Day detector instance
     * @param  {ShiftLightsTechnique} technique        Shift Lights illumination technique
     *
     * @return void
     */
    public void display(
      double               rpmPct,
      NightDayDetector     nightDayDetector,
      ShiftLightsTechnique technique
    ) {
      this.setLedState(technique.illuminate(
        this.scalingTechnique.scale(rpmPct),
        this.scaledMinRpmPct,
        this.scaledMaxRpmPct,
        this.nightMode == NightMode.Auto
          ? nightDayDetector.IsNight()
          : this.nightMode == NightMode.On
      ));
    }

    /**
     * Preview the current Rev LEDs settings
     *
     * @param  {ShiftLightsTechnique} technique        The current Shift Lights Technique
     * @param  {int}                  frameDelayMs     The delay between frames in milliseconds
     * @param  {NightDayDetector}     nightDayDetector Night/Day detector instance
     * @param  {CancellationToken}    cancel           Cancellation token to stop preview while running
     *
     * @return Task
     */
    public async Task Preview(
      ShiftLightsTechnique technique,
      int                  frameDelayMs,
      NightDayDetector     nightDayDetector,
      CancellationToken    cancel
    ) {
      // Increase RPM percentage by one raw percent per frame in order to display
      const double onePercent = 0.01;

      try {
        double i = 0;
        while (!this.IsAtRedline(i)) {
          this.display(i, nightDayDetector, technique);

          i += onePercent;

          await Task.Delay(frameDelayMs, cancel);
        }

        while (i >= 0) {
          this.display(i, nightDayDetector, technique);

          i -= onePercent;

          await Task.Delay(frameDelayMs, cancel);
        }
      } catch {
        // Do nothing, preview was cancelled by user
      }
    }
  }
}
